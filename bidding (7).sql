-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 09, 2021 at 06:47 PM
-- Server version: 5.7.33-0ubuntu0.16.04.1
-- PHP Version: 7.3.27-9+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bidding`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_processings`
--

CREATE TABLE `cart_processings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `total_price` bigint(20) UNSIGNED NOT NULL,
  `qun` int(11) NOT NULL,
  `single_price` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `close_bids`
--

CREATE TABLE `close_bids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `close_bids`
--

INSERT INTO `close_bids` (`id`, `user_id`, `product_id`, `order_id`, `created_at`, `updated_at`) VALUES
(12, 13, 29, 52, '2021-09-04 04:59:55', '2021-09-04 04:59:55'),
(13, 15, 28, 44, '2021-09-04 05:00:40', '2021-09-04 05:00:40'),
(14, 13, 30, 56, '2021-09-13 07:12:32', '2021-09-13 07:12:32'),
(15, 13, 31, 55, '2021-09-14 11:44:32', '2021-09-14 11:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fcm_driver_tokens`
--

CREATE TABLE `fcm_driver_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fcm_user_tokens`
--

CREATE TABLE `fcm_user_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fcm_user_tokens`
--

INSERT INTO `fcm_user_tokens` (`id`, `token`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'cOk9ar5FQxqUKPY61i0680:APA91bERyp6KmGypE9pGqZkjUChL8uC7wLx74HHglCQZqxPHJvZ3wn4ptHvJp6IZpxjQrD6N7goWs7rPk8fJNfSrMRbi0VEAi00Rpm7HlORvdnvQ90Nn7gNJLybxc2dJewMvuRce3NPH', 12, '2021-10-09 08:09:58', '2021-10-09 08:09:58');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `description`, `image`, `trash`, `created_at`, `updated_at`) VALUES
(13, 'Earn Money', 'Earn Money', 'menu_images/1626680816/0ezUOiZkNMK16W14QgQdRiSBihfD4Her46FgJE7D.png', 0, '2021-07-19 02:16:56', '2021-07-19 02:16:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_27_110018_add_fb_id_column_in_users_table', 1),
(5, '2021_04_16_122730_user_fields', 1),
(6, '2021_04_23_055931_create_products_table', 2),
(7, '2021_04_23_061102_create_menus_table', 2),
(8, '2021_04_30_044527_create_cart_processings_table', 3),
(9, '2021_04_30_064114_create_orders_table', 3),
(10, '2021_04_30_064504_orders_constrait', 3),
(11, '2021_05_10_135149_create_drivers_table', 4),
(12, '2021_05_11_060247_create_fcm_user_tokens_table', 4),
(13, '2021_05_11_060618_create_user_addresses_table', 4),
(14, '2021_05_11_065147_forcon', 4),
(15, '2021_05_14_133346_order_shipping_col', 4),
(16, '2021_05_14_135348_order_comment', 4),
(17, '2021_05_14_151130_trash_col_driver_and_user', 4),
(18, '2021_05_14_153240_order_status', 4),
(19, '2021_05_14_164444_order_keys_driver', 4),
(20, '2021_05_15_120329_create_fcm_driver_tokens_table', 4),
(21, '2021_05_22_102358_otpforuser', 4),
(22, '2021_05_23_142115_update_driver_location_order', 4),
(23, '2021_07_19_040956_productcols', 5),
(24, '2021_07_19_050621_order_del', 6),
(25, '2021_07_19_051058_order_add_product_id', 7),
(26, '2021_07_19_073527_productprice', 8),
(27, '2021_08_03_114913_product_landing_page', 9),
(28, '2021_08_05_065611_create_wallets_table', 10),
(29, '2021_08_05_120701_user_wallet_col', 11),
(30, '2021_09_04_071253_create_close_bids_table', 12),
(31, '2021_09_10_094026_create_wallet_payments_table', 13),
(32, '2021_09_13_103334_wallet_payment_col_user_id', 14),
(33, '2021_09_13_105229_wallet_payment_col_payment_paid', 15),
(34, '2021_09_27_162526_create_sliders_table', 15),
(35, '2021_10_09_113617_create_notification_users_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_users`
--

INSERT INTO `notification_users` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'What is Lorem Ipsum?\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2021-10-09 06:12:42', '2021-10-09 06:12:42'),
(2, 'What is Lorem Ipsum?\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2021-10-09 06:12:42', '2021-10-09 06:12:42'),
(3, 'Where can I get some?\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2021-10-09 06:12:42', '2021-10-09 06:12:42'),
(4, 'What is Lorem Ipsum?\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2021-10-09 06:12:42', '2021-10-09 06:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_price` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `payment_recieved` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `number` double NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total_price`, `user_id`, `payment_recieved`, `payment_id`, `created_at`, `updated_at`, `status`, `number`, `product_id`) VALUES
(1, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:44', '2021-07-23 01:40:44', 'pending', 10.64, 28),
(2, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:44', '2021-07-23 01:40:44', 'pending', 10.64, 28),
(3, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:46', '2021-07-23 01:40:46', 'pending', 10.64, 28),
(4, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:37', '2021-07-23 02:08:37', 'pending', 98, 28),
(5, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:37', '2021-07-23 02:08:37', 'pending', 98, 28),
(6, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:38', '2021-07-23 02:08:38', 'pending', 98, 28),
(7, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:38', '2021-07-23 02:08:38', 'pending', 98, 28),
(8, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:10:32', '2021-07-23 02:10:32', 'pending', 895, 28),
(9, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:12:10', '2021-07-23 02:12:10', 'pending', 10, 28),
(10, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:36:14', '2021-07-23 02:36:14', 'pending', 89, 28),
(11, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:37:03', '2021-07-23 02:37:03', 'pending', 12.6, 28),
(12, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:41:19', '2021-07-23 02:41:19', 'pending', 10.64, 28),
(13, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:44:32', '2021-07-23 02:44:32', 'pending', 16.646, 28),
(14, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:21', '2021-07-23 02:45:21', 'pending', 10.25, 28),
(15, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:38', '2021-07-23 02:45:38', 'pending', 10.25, 28),
(16, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:56', '2021-07-23 02:45:56', 'pending', 10.63, 28),
(17, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:46:08', '2021-07-23 02:46:08', 'pending', 10.94, 28),
(18, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:46:28', '2021-07-23 02:46:28', 'pending', 10.64, 28),
(19, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-24 02:42:18', '2021-07-24 02:42:18', 'pending', 10.8, 29),
(20, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-24 02:42:18', '2021-07-24 02:42:18', 'pending', 10.8, 29),
(21, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 02:18:47', '2021-07-28 02:18:47', 'pending', 89.52, 29),
(22, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 02:18:56', '2021-07-28 02:18:56', 'pending', 55.4, 29),
(23, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 02:19:07', '2021-07-28 02:19:07', 'pending', 52.1, 29),
(24, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 05:35:58', '2021-07-28 05:35:58', 'pending', 40.5, 28),
(25, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 05:36:12', '2021-07-28 05:36:12', 'pending', 48.8, 28),
(26, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 05:44:12', '2021-07-28 05:44:12', 'pending', 469, 29),
(27, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 05:44:16', '2021-07-28 05:44:16', 'pending', 9464, 29),
(28, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 05:44:22', '2021-07-28 05:44:22', 'pending', 67.643, 29),
(29, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 06:30:25', '2021-07-28 06:30:25', 'pending', 18.2696392, 28),
(30, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 06:38:27', '2021-07-28 06:38:27', 'pending', 10.2, 29),
(31, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 06:48:25', '2021-07-28 06:48:25', 'pending', 5.23, 29),
(32, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 06:48:40', '2021-07-28 06:48:40', 'pending', 3.64, 29),
(33, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-28 23:21:21', '2021-07-28 23:21:21', 'pending', 2.52, 29),
(34, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 02:50:17', '2021-08-03 02:50:17', 'pending', 25, 29),
(35, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 02:50:21', '2021-08-03 02:50:21', 'pending', 66, 29),
(36, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 02:50:26', '2021-08-03 02:50:26', 'pending', 55.5, 29),
(37, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 02:50:32', '2021-08-03 02:50:32', 'pending', 62.5, 29),
(38, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 02:58:05', '2021-08-03 02:58:05', 'pending', 85.48, 29),
(39, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 04:04:52', '2021-08-03 04:04:52', 'pending', 16, 29),
(40, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 04:07:44', '2021-08-03 04:07:44', 'pending', 5, 29),
(41, 10, 14, 0, 'nwiji786ryh8467974w9', '2021-08-03 06:53:25', '2021-08-03 06:53:25', 'pending', 31, 29),
(42, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-08-03 07:09:55', '2021-08-03 07:09:55', 'pending', 81, 29),
(43, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-08-03 07:10:05', '2021-08-03 07:10:05', 'pending', 4.64, 29),
(44, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-08-03 07:10:24', '2021-08-03 07:10:24', 'pending', 1.5, 28),
(45, 10, 12, 0, 'nwiji786ryh8467974w9', '2021-08-05 08:00:03', '2021-08-05 08:00:03', 'pending', 10, 29),
(46, 10, 12, 0, 'nwiji786ryh8467974w9', '2021-08-05 08:00:15', '2021-08-05 08:00:15', 'pending', 101, 29),
(47, 10, 12, 0, 'nwiji786ryh8467974w9', '2021-08-05 08:01:19', '2021-08-05 08:01:19', 'pending', 48, 29),
(48, 10, 12, 0, 'nwiji786ryh8467974w9', '2021-08-05 08:01:23', '2021-08-05 08:01:23', 'pending', 46, 29),
(49, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 02:31:04', '2021-09-02 02:31:04', 'pending', 10, 29),
(50, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 02:31:09', '2021-09-02 02:31:09', 'pending', 1.96, 29),
(51, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 02:31:14', '2021-09-02 02:31:14', 'pending', 4.67, 29),
(52, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 06:04:42', '2021-09-02 06:04:42', 'pending', 1.54, 29),
(53, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 06:04:49', '2021-09-02 06:04:49', 'pending', 1.6, 29),
(54, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 07:02:22', '2021-09-02 07:02:22', 'pending', 78, 29),
(55, 13000, 13, 0, 'nwiji786ryh8467974w9', '2021-09-02 07:17:36', '2021-09-02 07:17:36', 'pending', 5, 31),
(56, 12000, 13, 0, 'nwiji786ryh8467974w9', '2021-09-13 07:11:59', '2021-09-13 07:11:59', 'pending', 205, 31),
(57, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-09-17 06:42:00', '2021-09-17 06:42:00', 'pending', 5.2, 34),
(58, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-09-17 06:43:35', '2021-09-17 06:43:35', 'pending', 0.32, 34),
(59, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-09-17 06:46:16', '2021-09-17 06:46:16', 'pending', 1.25, 34),
(60, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-09-17 07:59:50', '2021-09-17 07:59:50', 'pending', 1.4, 34),
(61, 10, 15, 0, 'nwiji786ryh8467974w9', '2021-09-17 09:58:30', '2021-09-17 09:58:30', 'pending', 31.64, 34);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nutrition_qualities` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_from` datetime NOT NULL,
  `start_to` datetime NOT NULL,
  `bidprice` double NOT NULL,
  `image_landing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `image`, `trash`, `popular`, `menu_id`, `nutrition_qualities`, `created_at`, `updated_at`, `start_from`, `start_to`, `bidprice`, `image_landing`) VALUES
(28, 'Brand New Acer Laptop', 'Acer Aspire 5 Thin and light laptop intel core i5 11th gen (8GB/1 TB HDD/ Windows 10 home) A515-56 With 39.6 cm (15.6 inch) with FHD', 40000, 'menu_images/1627979077/gjdyfeEHcETM7bEtKLRY0p6E6SNgD7hY10hAe5Pk.png', 1, 0, 13, NULL, '2021-07-19 02:16:36', '2021-10-09 09:29:46', '2021-07-23 12:00:00', '2021-07-23 13:00:00', 10, 'menu_images_landing/1627991428/X8DgPZ2NfAz6M1h7xXvf2JasWxmcx2OvPaw5tbsb.png'),
(29, 'Brand New IPhone X', 'It’s safe to say that the iPhone X has been the most-talked about tech product of 2017. The anticipation for the tenth anniversary iPhone started last year, well before the usual time for the iPhone rumour mill to start whirring', 38000, 'menu_images/1627978938/tOh2EiQj149CUu5MsEUkSIFsKRMWF31gN9JI49mx.png', 1, 0, 13, NULL, '2021-07-23 02:52:11', '2021-10-09 09:29:47', '2021-07-23 02:00:00', '2021-07-24 03:00:00', 10, 'menu_images_landing/1627991446/Dmm3iH3GTBcJCJEjEU8iAYbq9WoGccrp93ZwAZvr.png'),
(30, 'Brand New IPhone X', 'It’s safe to say that the iPhone X has been the most-talked about tech product of 2017. The anticipation for the tenth anniversary iPhone started last year, well before the usual time for the iPhone rumour mill to start whirring', 40000, 'menu_images/1630750307/GaSYgQOLxp5i8JTZoc6bA2brDU6YqLW5yQf9aGNL.png', 1, 0, 13, NULL, '2021-09-04 04:41:15', '2021-09-13 07:18:48', '2021-09-04 20:20:00', '2021-09-05 20:00:00', 10, 'menu_images_landing/1630750307/uhaB6zfUsdlDoX556uOl1VVufmFjQdgnRnVrsYcP.png'),
(31, 'Win Iphone', 'Win Iphone', 15000, 'menu_images/1631537473/KOnf2v5AAftqmQVw6I1pD5Q6KxM3jCJErDJPr49U.png', 1, 0, 13, NULL, '2021-09-13 07:20:06', '2021-10-09 08:23:27', '2021-09-13 18:20:00', '2021-09-14 16:20:00', 10, 'menu_images_landing/1631537473/0KHRuW79lZT6xvyYGEKl74KRRMSKXukPoLsvTNuJ.png'),
(32, 'Win Iphone', 'Win Iphone', 10000, NULL, 1, 0, 13, NULL, '2021-09-14 12:04:40', '2021-10-09 10:27:17', '2021-09-14 18:00:00', '2021-10-09 15:57:17', 10, NULL),
(33, 'Win Iphone', 'Win Iphone', 10000, 'menu_images/1631621167/3dqohzsiBkauw3z5fbCiNYOPYyhGx4Lv3891i6oY.png', 0, 0, 13, NULL, '2021-09-14 12:06:07', '2021-10-09 10:06:15', '2021-09-14 18:00:00', '2021-09-16 00:00:00', 10, 'menu_images_landing/1631621167/3dqohzsiBkauw3z5fbCiNYOPYyhGx4Lv3891i6oY.png'),
(34, 'Iphone 10', 'Iphone 10', 15000, 'menu_images/1631860859/QJqXp6lhzjCtARwGGMrlgL85VdvJV1KK9PeasV5O.png', 0, 0, 13, NULL, '2021-09-17 06:40:59', '2021-10-09 10:06:15', '2021-09-17 12:10:00', '2021-09-19 01:10:00', 10, 'menu_images_landing/1631860860/QJqXp6lhzjCtARwGGMrlgL85VdvJV1KK9PeasV5O.png'),
(35, 'Iphone 10', 'The iPhone X display has rounded corners that follow a beautiful curved design, and these corners are within a standard rectangle.', 40000, 'menu_images/1633767674/vKe0uCoyAFsbUCElAyrRlzTt7Q1QiTYkdXWqyKlr.png', 0, 0, 13, NULL, '2021-10-09 08:21:14', '2021-10-09 08:21:15', '2021-10-09 12:00:00', '2021-10-10 12:00:00', 10, 'menu_images_landing/1633767675/vKe0uCoyAFsbUCElAyrRlzTt7Q1QiTYkdXWqyKlr.png');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `path`, `trash`, `created_at`, `updated_at`) VALUES
(1, 'sliders/1633758014/QtcN4dmwnoiRV4KPfuJm89tJpFRXCu2raBkfJCP5.png', 0, '2021-10-09 05:40:15', '2021-10-09 05:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fb_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `otp` int(11) DEFAULT NULL,
  `otp_attempt` int(11) NOT NULL DEFAULT '0',
  `otp_expire` datetime DEFAULT NULL,
  `wallet` double NOT NULL DEFAULT '0',
  `admin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `fb_id`, `address`, `mobile`, `verify`, `token`, `trash`, `otp`, `otp_attempt`, `otp_expire`, `wallet`, `admin`) VALUES
(1, 'Gurinder', 'gurinder@domainandspace.com', NULL, '123456', NULL, '2021-04-17 06:01:54', '2021-04-17 06:01:54', NULL, 'Sanrgrur', '9646848434', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(3, 'Gurinder', 'gurinder@domainandspace.com1', NULL, '123456780', NULL, '2021-04-17 06:09:39', '2021-04-17 06:09:39', NULL, 'Sanrgrur', '96468484340', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(4, 'Gurinder', 'gurinder@domainandspace1.com1', NULL, '123456780', NULL, '2021-04-17 06:10:24', '2021-04-17 06:10:24', NULL, 'Sanrgrur1', '9646848411340', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(5, 'Gurinder', 'gurinder@domainandspace1.c', NULL, '$2y$10$s99wcJ.k8zysftf4woG.BeVEvYvQ0.nALY/oCDy415PrO17oIAcYW', NULL, '2021-04-17 06:31:00', '2021-04-17 06:31:00', NULL, 'Sanrgrur1', '96468484113', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(6, 'Gurinder', 'gurinder1@domainandspace.com', NULL, '$2y$10$/MxNqT1QXXWavZZQLz0PC.4EnDhbKcxBTxJ4KunWBAFZxtT5dwNzq', NULL, '2021-04-21 05:47:02', '2021-04-21 05:47:02', NULL, 'Sangrur', '1234567890', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(7, 'Gurinder', 'gurinder12@domainandspace.com', NULL, '$2y$10$3inZZiaEwj1yVBRstBvihupMV.k1m3ff0PrKVSjajOL/oKBHjk.3m', NULL, '2021-04-21 06:07:49', '2021-04-21 06:07:50', NULL, 'Sangrur', '1234567891', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(8, 'Gurinder', 'gurinderbatth08@gmail.com', NULL, '$2y$10$iacZJmqpFaXtSXrJjVKD3.aliAuVy8Q0KOWaIMMTRZpuMslVKhL/C', NULL, '2021-04-21 06:45:48', '2021-04-21 06:45:49', NULL, 'Sangrur', '9646848431', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(9, 'Gurinder', 'Gurinderbatth228@gmail.com', NULL, '$2y$10$Eept9cdc/aEjURjXFqPXReI9/Z94.HY0yojszUsoSGgPhXfmxCcbi', NULL, '2021-04-21 06:49:04', '2021-04-21 06:49:04', NULL, 'Sangrur', '9646848419', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(10, 'Gurinder', 'gurinderbah08@gmail.com', NULL, '$2y$10$akAzhx.iXktTBWAETkqd2OJDs1gW8vhRz19yCozGLBTXkM72aozdG', NULL, '2021-04-21 06:51:44', '2021-08-05 06:40:25', NULL, 'Sangrur', '9646848492', 0, NULL, 0, NULL, 0, NULL, 21, 0),
(11, 'Gurinder', 'test@test.com', NULL, '$2y$10$oj3cDWjRcTbQyaLqGOb0je9Eo1k9bW0Cev4Po8lub7GSUyQoSCsF6', NULL, '2021-04-22 06:25:37', '2021-04-22 06:25:38', NULL, 'Sangrur', '12345678', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(12, 'Gurinder', 'user@user.com', NULL, '$2y$10$1pQ.vHVpa1kTSmgUZcwraOA33uL147yUqtEwt5VtVDgFlWvXg6RUm', NULL, '2021-04-22 06:28:04', '2021-08-07 03:11:36', NULL, 'Sangrur', '1234512345', 0, NULL, 0, NULL, 0, NULL, 6792.88, 0),
(13, 'Gurinder', 'Gurinderbatth@gmail.com', NULL, '$2y$10$zsfxMP0G4h5dLKhiTHE3BuJZb24GELGxmMvz8uAyZZF1NmWfA74nW', NULL, '2021-07-19 06:00:26', '2021-09-14 11:44:32', NULL, 'Sangrur', '9530652160', 0, NULL, 0, NULL, 0, NULL, 6885.84, 0),
(14, 'William', 'William@domainandspace.com', NULL, '$2y$10$ft3BiILf/CQOTwHiWW2jkOp5u8.YeJ1j6lVmjx2HZAFEVRMWApXNe', NULL, '2021-07-31 04:40:12', '2021-07-31 04:40:12', NULL, NULL, '9530652162', 0, NULL, 0, NULL, 0, NULL, 0, 0),
(15, 'Sunil Nanda', 'Sunil@domainandspace.com', NULL, '$2y$10$t8oHxzkMztlpBMYSy2Tkz.IBTfXTyeV8NMyr8u1U3xBr0vHDmXBSi', NULL, '2021-08-03 07:00:43', '2021-09-17 09:58:31', NULL, NULL, '9530463431', 0, NULL, 0, NULL, 0, NULL, 928.5, 0),
(16, 'admin', 'admin@admin.com', '2021-09-14 02:57:55', '$2y$10$UvmTuR6Q6g3f9QX51P/e1ukDuuW/BEm7Ty.HXCkipMwnqtD8p.1L6', 'Cp5PE6ZJ2Im6676Iv9ZWolrsy9LRodjTMTTBVJiUIIPjpoIoXhZskVv1nHVt', '2021-09-14 02:57:56', '2021-09-14 02:57:56', NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `street_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `amount`, `reason`, `created_at`, `updated_at`) VALUES
(1, 10, 10, 'Added by payment gateway', '2021-08-05 06:23:08', '2021-08-05 06:23:08'),
(2, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:23:23', '2021-08-05 06:23:23'),
(3, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:28:57', '2021-08-05 06:28:57'),
(4, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:30:33', '2021-08-05 06:30:33'),
(5, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:40:05', '2021-08-05 06:40:05'),
(6, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:40:18', '2021-08-05 06:40:18'),
(7, 10, 10.5, 'Added by payment gateway', '2021-08-05 06:40:25', '2021-08-05 06:40:25'),
(8, 12, 10.8, 'Added by payment gateway', '2021-08-05 07:25:11', '2021-08-05 07:25:11'),
(9, 12, 78, 'Added by payment gateway', '2021-08-05 07:25:32', '2021-08-05 07:25:32'),
(10, 12, 28, 'Added by payment gateway', '2021-08-05 07:28:14', '2021-08-05 07:28:14'),
(11, 12, 30, 'Added by payment gateway', '2021-08-05 07:28:19', '2021-08-05 07:28:19'),
(12, 12, 10.5, 'Added by payment gateway', '2021-08-05 07:28:23', '2021-08-05 07:28:23'),
(13, 12, 64.8, 'Added by payment gateway', '2021-08-05 07:28:27', '2021-08-05 07:28:27'),
(14, 12, 138, 'Added by payment gateway', '2021-08-05 07:28:39', '2021-08-05 07:28:39'),
(15, 12, 15.64, 'Added by payment gateway', '2021-08-05 07:28:42', '2021-08-05 07:28:42'),
(16, 12, 43, 'Added by payment gateway', '2021-08-05 07:28:50', '2021-08-05 07:28:50'),
(17, 12, 49.64, 'Added by payment gateway', '2021-08-05 07:28:55', '2021-08-05 07:28:55'),
(18, 12, 466, 'Added by payment gateway', '2021-08-05 07:28:59', '2021-08-05 07:28:59'),
(19, 12, 97, 'Added by payment gateway', '2021-08-05 07:29:02', '2021-08-05 07:29:02'),
(20, 12, 94, 'Added by payment gateway', '2021-08-05 07:29:04', '2021-08-05 07:29:04'),
(21, 12, 100, 'Added by payment gateway', '2021-08-05 07:59:15', '2021-08-05 07:59:15'),
(22, 12, 100, 'Added by payment gateway', '2021-08-05 07:59:26', '2021-08-05 07:59:26'),
(23, 12, 20, 'Added by payment gateway', '2021-08-05 07:59:29', '2021-08-05 07:59:29'),
(24, 12, 64, 'Added by payment gateway', '2021-08-05 07:59:33', '2021-08-05 07:59:33'),
(25, 12, 49, 'Added by payment gateway', '2021-08-05 07:59:35', '2021-08-05 07:59:35'),
(26, 12, 3464, 'Added by payment gateway', '2021-08-05 07:59:38', '2021-08-05 07:59:38'),
(27, 12, 16, 'Added by payment gateway', '2021-08-06 01:52:55', '2021-08-06 01:52:55'),
(28, 12, 58, 'Added by payment gateway', '2021-08-06 04:33:20', '2021-08-06 04:33:20'),
(29, 12, 450, 'Added by payment gateway', '2021-08-06 04:33:26', '2021-08-06 04:33:26'),
(30, 12, 28, 'Added by payment gateway', '2021-08-06 04:33:29', '2021-08-06 04:33:29'),
(31, 12, 108, 'Added by payment gateway', '2021-08-06 04:33:36', '2021-08-06 04:33:36'),
(32, 12, 1000, 'Added by payment gateway', '2021-08-06 04:33:39', '2021-08-06 04:33:39'),
(33, 12, 200, 'Added by payment gateway', '2021-08-06 04:34:05', '2021-08-06 04:34:05'),
(34, 12, 10.5, 'Added by payment gateway', '2021-08-07 03:11:36', '2021-08-07 03:11:36'),
(35, 15, 10000, 'cash payment', '2021-08-11 01:36:01', '2021-08-11 01:36:01'),
(36, 15, 10, 'dd', '2021-08-14 02:25:15', '2021-08-14 02:25:15'),
(37, 15, -10, 'dsds', '2021-08-14 02:55:38', '2021-08-14 02:55:38'),
(38, 15, 1000, 'test', '2021-08-14 04:00:25', '2021-08-14 04:00:25'),
(39, 15, -20, 'test', '2021-08-14 04:00:37', '2021-08-14 04:00:37'),
(40, 13, 1000, 'Added by payment gateway', '2021-09-02 02:30:18', '2021-09-02 02:30:18'),
(41, 13, 500, 'Added by payment gateway', '2021-09-02 02:30:24', '2021-09-02 02:30:24'),
(42, 13, 3000, 'Added by payment gateway', '2021-09-02 02:30:29', '2021-09-02 02:30:29'),
(43, 13, 10000, 'Added by payment gateway', '2021-09-02 02:30:33', '2021-09-02 02:30:33'),
(44, 13, 12, 'Added by payment gateway', '2021-09-02 06:04:23', '2021-09-02 06:04:23'),
(45, 13, -1.54, 'Brand New IPhone X for winning', '2021-09-04 04:59:55', '2021-09-04 04:59:55'),
(46, 15, -1.5, 'Brand New Acer Laptop for winning', '2021-09-04 05:00:40', '2021-09-04 05:00:40'),
(47, 13, 300, 'Added by payment gateway', '2021-09-13 05:20:13', '2021-09-13 05:20:13'),
(48, 13, 300, 'Added by payment gateway', '2021-09-13 05:20:59', '2021-09-13 05:20:59'),
(49, 13, 300, 'Added by payment gateway', '2021-09-13 05:21:04', '2021-09-13 05:21:04'),
(50, 13, 300, 'Added by payment gateway', '2021-09-13 05:21:07', '2021-09-13 05:21:07'),
(51, 13, 300, 'Added by payment gateway', '2021-09-13 05:24:45', '2021-09-13 05:24:45'),
(52, 13, 5000, 'Added by payment gateway', '2021-09-13 06:51:37', '2021-09-13 06:51:37'),
(53, 13, 500, 'Added by payment gateway', '2021-09-13 06:55:03', '2021-09-13 06:55:03'),
(54, 13, 100, 'Added by payment gateway', '2021-09-13 07:02:48', '2021-09-13 07:02:48'),
(55, 13, 10, 'Added by payment gateway', '2021-09-13 07:04:50', '2021-09-13 07:04:50'),
(56, 13, -205, 'Brand New IPhone X for winning', '2021-09-13 07:12:32', '2021-09-13 07:12:32'),
(57, 13, -5, 'Win Iphone for winning', '2021-09-14 11:44:32', '2021-09-14 11:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `wallet_payments`
--

CREATE TABLE `wallet_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wallet_payments`
--

INSERT INTO `wallet_payments` (`id`, `amount`, `paid`, `payment_id`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 100, 0, 'order_Hvm7ZojZPTjwUS', '2021-09-10 07:45:25', '2021-09-10 07:45:25', 0),
(2, 100, 0, 'order_Hvm7viC4UJi2Aq', '2021-09-10 07:45:45', '2021-09-10 07:45:45', 0),
(3, 100, 0, 'order_Hw4BwJSgEiv2PD', '2021-09-11 01:26:02', '2021-09-11 01:26:02', 0),
(4, 100, 0, 'order_Hw4DGQOnnLSbhJ', '2021-09-11 01:27:17', '2021-09-11 01:27:17', 0),
(5, 100, 0, 'order_Hw4QfdEKitlt2l', '2021-09-11 01:39:58', '2021-09-11 01:39:58', 0),
(6, 100, 0, 'order_Hw4Qqf7f9epBWX', '2021-09-11 01:40:09', '2021-09-11 01:40:09', 0),
(7, 100, 0, 'order_Hw4SbI5ET9wMjC', '2021-09-11 01:41:48', '2021-09-11 01:41:48', 0),
(8, 100, 0, 'order_Hw7s84hg2rAtMX', '2021-09-11 05:02:03', '2021-09-11 05:02:03', 0),
(9, 100, 0, 'order_Hw8eftau3fozFz', '2021-09-11 05:48:00', '2021-09-11 05:48:00', 0),
(10, 100, 0, 'order_Hw8kHCpg16gTSt', '2021-09-11 05:53:18', '2021-09-11 05:53:18', 0),
(11, 100, 0, 'order_HwthEdefjYADQN', '2021-09-13 03:49:02', '2021-09-13 03:49:02', 0),
(12, 300, 0, 'order_HwuVlWKpOUES5d', '2021-09-13 04:36:52', '2021-09-13 04:36:52', 0),
(13, 1080, 0, 'order_HwuW8EJws1cK3w', '2021-09-13 04:37:13', '2021-09-13 04:37:13', 0),
(14, 400, 0, 'order_HwuWUdcAHsoZeS', '2021-09-13 04:37:33', '2021-09-13 04:37:33', 0),
(15, 500, 0, 'order_HwuYf2hQQcOs0Y', '2021-09-13 04:39:36', '2021-09-13 04:39:36', 0),
(16, 500, 0, 'order_HwucUlNgPwTyfW', '2021-09-13 04:43:14', '2021-09-13 04:43:14', 0),
(17, 500, 0, 'order_HwucqiCga7R1tP', '2021-09-13 04:43:34', '2021-09-13 04:43:34', 0),
(18, 500, 0, 'order_HwudFoxdqNc9Ck', '2021-09-13 04:43:57', '2021-09-13 04:43:57', 0),
(19, 20000, 0, 'order_HwufearX9EiJ0W', '2021-09-13 04:46:13', '2021-09-13 04:46:13', 0),
(20, 20000, 0, 'order_HwukUzHPLnHAgt', '2021-09-13 04:50:49', '2021-09-13 04:50:49', 0),
(21, 1000, 0, 'order_Hwuo9Nf8t3khwO', '2021-09-13 04:54:16', '2021-09-13 04:54:16', 0),
(22, 300, 1, 'order_HwvAxMVIZKKiII', '2021-09-13 05:15:51', '2021-09-13 05:24:45', 13),
(23, 10000, 0, 'order_HwwW7B9VCY3fbn', '2021-09-13 06:34:35', '2021-09-13 06:34:35', 13),
(24, 5000, 0, 'order_HwwYssrpEyI3kR', '2021-09-13 06:37:12', '2021-09-13 06:37:12', 13),
(25, 500, 0, 'order_HwwakLSUmdE8mZ', '2021-09-13 06:38:58', '2021-09-13 06:38:58', 13),
(26, 500, 0, 'order_Hwwj9HPTqY7oGn', '2021-09-13 06:46:55', '2021-09-13 06:46:55', 13),
(27, 2000, 0, 'order_Hwwl2jkF8SBae8', '2021-09-13 06:48:43', '2021-09-13 06:48:43', 13),
(28, 5000, 1, 'order_HwwnAObg8RB1uO', '2021-09-13 06:50:43', '2021-09-13 06:51:37', 13),
(29, 500, 1, 'order_HwwrBSS7O6Gayf', '2021-09-13 06:54:32', '2021-09-13 06:55:03', 13),
(30, 10, 0, 'order_HwwtAcw4VTd2Mc', '2021-09-13 06:56:24', '2021-09-13 06:56:24', 13),
(31, 10000, 0, 'order_Hwww2w6iysEvz3', '2021-09-13 06:59:08', '2021-09-13 06:59:08', 13),
(32, 10000, 0, 'order_HwwxGLCs7jqkZq', '2021-09-13 07:00:17', '2021-09-13 07:00:17', 13),
(33, 100, 1, 'order_HwwzPfNtvAWeMa', '2021-09-13 07:02:19', '2021-09-13 07:02:48', 13),
(34, 10, 0, 'order_Hwx134M4TZG9Jw', '2021-09-13 07:03:52', '2021-09-13 07:03:52', 13),
(35, 10, 1, 'order_Hwx1XaSqJPsszc', '2021-09-13 07:04:20', '2021-09-13 07:04:50', 13),
(36, 100, 0, 'order_HzcjkalrzYJJC7', '2021-09-20 06:40:37', '2021-09-20 06:40:37', 15),
(37, 100, 0, 'order_HzcjzPe4sXltma', '2021-09-20 06:40:51', '2021-09-20 06:40:51', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_processings`
--
ALTER TABLE `cart_processings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_processings_order_id_foreign` (`order_id`),
  ADD KEY `cart_processings_user_id_foreign` (`user_id`);

--
-- Indexes for table `close_bids`
--
ALTER TABLE `close_bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fcm_driver_tokens_driver_id_foreign` (`driver_id`);

--
-- Indexes for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fcm_user_tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_payments`
--
ALTER TABLE `wallet_payments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_processings`
--
ALTER TABLE `cart_processings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `close_bids`
--
ALTER TABLE `close_bids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `wallet_payments`
--
ALTER TABLE `wallet_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_processings`
--
ALTER TABLE `cart_processings`
  ADD CONSTRAINT `cart_processings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_processings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  ADD CONSTRAINT `fcm_driver_tokens_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `drivers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  ADD CONSTRAINT `fcm_user_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
