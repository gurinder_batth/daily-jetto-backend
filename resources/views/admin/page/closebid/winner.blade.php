@extends($ADMIN_EXTEND)
@section('content')

<div class="card">
    <div class="row">
         <div class="col-md-12 table-responsive">
               <table class="table table-striped table-hover table-border text-center">
                    <tr class="bg-primary text-white">
                      <th>Order Id</th>    
                      <th>User</th>    
                      <th>Product</th>    
                      <th>Number</th>    
                      <th>Bet Price</th>    
                      <th>Date</th>    
                    </tr>    
                    @foreach ($list as $item)
                    <tr>
                        <td> {{$item->id}} </td>
                        <td> {{$item->user->name}} </td>
                        <td> {{$item->product->title}} </td>
                        <td> {{$item->order->number}} </td>
                        <td> {{$item->order->total_price}} </td>
                        <td> {{$item->created_at->format("h:i a d-M-y")}} </td>
                    </tr>
                    @endforeach
               </table>
         </div>
    </div>
</div>
@endsection
