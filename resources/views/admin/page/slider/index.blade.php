@extends($ADMIN_EXTEND)
@section('content')

<div class="card">
    <div class="card-header">
        <form action="{{a_route('slider.create')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">Slider Image</label>
                <input type="file" class="form-control" id="upload_img_landing" name="temp_image" style="display:none" onchange="document.getElementById('productimg_landing_file').innerHTML = this.files[0].name"  required>
                <p id="productimg_landing_file"></p>
                <button type="button" class="btn btn-primary btn-sm" onclick="document.getElementById('upload_img_landing').click()">Upload Image <i class="fa fa-image"></i> </button>
             </div>
    
            <div class="form-group text-left">
                <button class="btn btn-success btn-sm">
                    Submit
                </button>
            </div>
            
        </form>
    </div>
    <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>Img</th>
                    <th>Hide/Show</th>
                </tr>
                @foreach($list as $key => $item)
                    <tr>
                        <td> {{$item->id}} </td>
                        <td> {{$item->created_at->format("d-M-y")}} </td>
                        <td> <img style="max-width:200px;max-height:200px" src="{{$item->img_path}}" alt=""> </td>
                        <td>

                          @if($item->trash == 0)   
                             <a href="{{ a_route('slider.trash' , ['id' => $item->id , 'flag' => 1 ]) }}" class="btn btn-success btn-sm">Show</a> 
                          @else
                            <a href="{{ a_route('slider.trash' , ['id' => $item->id , 'flag' => 0 ]) }}"  class="btn btn-danger btn-sm">Hide</a> 
                          @endif

                        </td>
                    </tr>
                @endforeach
            </table>
    </div>
</div>

@endsection
