@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">

    <div class="card-header card-header-success">
            <h4 class="card-title "> <i class="fa fa-motorcycle"></i> &nbsp Driver List</h4>
            <p class="card-category">Driver list from Planet Vegan APP</p>
            <div class="text-right">
            </div>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class="table   table-striped table-bordered">
                <tr>
                   <th>Id</th>
                   <th>Name</th>
                   <th>Mobile</th>
                   <th>Email</th>
                   <th>V.Name</th>
                   <th>V.Number</th>
                   <th>Action</th>
                </tr>
                @foreach ($drivers as $item)
                  <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->mobile}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->vehicle_name}}</td>
                    <td>{{$item->vehicle_number}}</td>
                    <td>
                        <a href="{{a_route('driver.password',['id' => $item->id])}}"
                            data-toggle="tooltip" data-placement="top" title="Reset the password of #{{$item->name}}"
                            class="btn btn-success btn-sm" style="padding: 0.40625rem 0.75rem;">
                            <i class="fa fa-lock"></i>
                        </a>
                    </td>
                  </tr>
                @endforeach
            </table>
        </div>
    </div>
 </div>

@endsection


@section('scripts')
  @include($inc."nq_script")
@endsection