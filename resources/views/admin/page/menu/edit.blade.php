@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">
   <div class="card-header">
      <h4>Add Menu</h4>
   </div>
    <div class="card-body">
         <form action="{{a_route('menu.update')}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Menu Title</label>
                       <input type="text" class="form-control" name="title" value="{{$menu->title}}" required>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Menu Description</label>
                       <input type="text" class="form-control" name="description" value="{{$menu->description}}"  required>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Proudct Image</label>
                       <input type="file" class="form-control" name="temp_image"  required>
                   </div>
               </div>
               @if($menu->image)
                 <img src="{{AWS_URL($menu->image)}}" style="max-width:200px"  alt="">  
               @endif
               <input type="hidden" name="id" value="{{$menu->id}}" id="">
               <div class="col-md-12 mt-2">
                   <div class="form-group text-left">
                       <button class="btn btn-primary">
                          Submit
                       </button>
                   </div>
               </div>
            </div>
         </form>
    </div>
 </div>

@endsection