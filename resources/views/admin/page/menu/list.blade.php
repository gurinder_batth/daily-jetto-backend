@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">

    <div class="card-header card-header-primary">
        <h4 class="card-title "> <i class="fa fa-user-plus"></i> &nbsp Category List</h4>
        <p class="card-category">Latest category list from APP</p>
        <div class="text-right">
        <a class="btn btn-white" href="{{a_route("menu.add")}}">Add Category</a>
        </div>
  </div>

    <div class="card-body">
       <div class="table-responsive">
        <table class="table  table-striped table-bordered">
            <tr>
                <th>Photo</th>
                <th>Title</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Trash</th>
            </tr>
            @foreach ($list as $item)
                <tr>
                    <td> <img src="{{AWS_URL($item->image)}}" style="max-width: 50px"  alt="">  </td>
                    <td> {{$item->title}} </td>
                    <td> {{$item->description}} </td>
                    <td> <a href="{{a_route('menu.edit',['id' => $item->id])}}" class="btn btn-sm btn-success" >Edit</a> </td>
                    <td> <a href="{{a_route('menu.delete',['id' => $item->id , 'flag' => 1])}}" class="btn btn-sm btn-danger" >Del</a> </td>
                </tr>
            @endforeach
      </table>
       </div>
    </div>
 </div>

@endsection