@extends($ADMIN_EXTEND)
@section('content')

<div class="card">

    <div class="card-header card-header-success">
        <h4 class="card-title "> <i class="fa fa-inr"></i> &nbsp Wallet {{$user->wallet}}</h4>
        <p class="card-category">Manage Wallet of {{$user->name}}</p>
        <div class="text-right">
        </div>
    </div>

    <div class="card-body">
        <form action="{{a_route('user.wallet.create')}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-inr"></i> <strong class="text-danger">*</strong>
                            </span>
                        </div>
                        <input type="number" min="1" class="form-control" placeholder="Amount" required name="amount"
                            value="{{old('amount')}}" aria-describedby="basic-addon1">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"> 
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-envelope"></i> <strong class="text-danger">*</strong>
                            </span>
                        </div>
                        <input type="text" class="form-control" placeholder="Reason" required name="reason"
                            value="{{old('reason')}}" aria-describedby="basic-addon1">
                        <input type="hidden" class="form-control" placeholder="Reason" required name="user_id"
                            value="{{request()->id}}" aria-describedby="basic-addon1">
                    </div>
                </div>

                <div class="col-md-12 ">
                    <div class="form-group text-left">
                        <button 
                          value="add"
                          class="btn btn-primary btn-sm btn-pv" name="submit" style="margin-left:2rem">
                          Add Amount
                        </button>
                        <button value="subtract" name="submit" class="btn  btn-sm btn-pv btn-danger" style="margin-left:2rem;background:red !important;color:white!important">
                          Subtract Amount
                        </button>
                    </div>
                </div>

            </div>
        </form>

        <div class="row">
            <div class="col-md-12">
                 <table class="table text-center table-bordered">
                      <tr>
                          <th>Date</th>
                          <th>Amount</th>
                          <th>C/D</th>
                          <th>Reason</th>
                      </tr>
                      @foreach ($wallet as $item)
                        <tr>
                            <td> {{$item->created_at->format("h:i A d-M-y")}} </td>
                            <td> {{$item->amount}} </td>
                            <td> 
                               @if($item->amount > 0)
                                  <b class="text-success">Credit</b>
                               @else
                                  <b class="text-danger">Debit</b>
                               @endif
                            </td>
                            <td>
                                {{$item->reason}}
                            </td>
                        </tr>
                      @endforeach
                 </table>
            </div>
        </div>
    </div>
</div>

@endsection
