@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title "> <i class="fa fa-user-plus"></i> &nbsp Bid List</h4>
        <p class="card-category">Latest bids list from APP</p>
        <div class="text-right">
        <a href="{{a_route("product.add")}}" class="btn btn-white">Add Bid</a>
        </div>
   </div>

    <div class="card-body">
       <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Photo</th>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Category</th>
                <th>Start</th>
                <th>Expired</th>
                <th>Edit</th>
                <th>Trash</th>
                <th></th>
            </tr>
            @foreach ($list as $item)
                <tr>
                    <td> <img src="{{AWS_URL($item->image)}}" style="max-width: 50px"  alt="">  </td>
                    <td> {{$item->title}} </td>
                    <td> {{$item->description}} </td>
                    <td> <div class="badge badge-success"> {{$item->price}} </div> </td>
                    <td> {{$item->menu->title ?? ""}} </td>
                    <td> {{$item->start_from->format("d-M-y h:i:a")}} </td>
                    <td> {{$item->start_to->format("d-M-y h:i:a")}} </td>
                    <td> <a href="{{a_route('product.edit',['id' => $item->id ])}}" class="btn btn-sm btn-success" >Edit</a> </td>
                    <td> <a onclick="return confirm('Are you sure?')" href="{{a_route('product.delete',['id' => $item->id , 'flag' => 1])}}" class="btn btn-sm btn-danger" >Del</a> </td>
                    <td> 
                        @if($item->closed == null)
                          <a onclick="return confirm('Are you sure?')" href="{{a_route('close_bid.close',['id' => $item->id ])}}" class="btn btn-sm btn-info" >Close</a>
                        @else
                          <b>Bid Closed</b>
                        @endif
                    </td>
                </tr>
            @endforeach
      </table>
       </div>
    </div>
 </div>

@endsection