@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">
   <div class="card-header">
      <h4>Edit Product  {{$product->title}} </h4>
      <div class="text-right">
            <a href="{{a_route("menu.add")}}">Add Category</a>
      </div>
   </div>
    <div class="card-body">
         <form action="{{a_route('product.update')}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Proudct Title</label>
                       <input type="text" class="form-control" name="title" value="{{$product->title}}" required>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Proudct Description</label>
                       <input type="text" class="form-control" name="description" value="{{$product->description}}"  required>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label for="">Proudct Price</label>
                       <input type="number" minlength="0" class="form-control" name="price"  value="{{$product->price}}" required>
                   </div>
               </div>
               
               <div class="col-md-4">
                <div class="form-group">
                    <label for="">Winning Amount</label>
                    <input type="number" minlength="0" class="form-control" name="bidprice" value="{{$product->bidprice}}" required>
                </div>
            </div>

            <div class="col-md-4 mt-2">
                <div class="form-group text-left">
                    <select name="menu_id" class="form-control" id="" style="margin-top: -10px;">
                       @foreach ($menu as $item)
                           <option value="{{$item->id}}" @if($product->menu_id == $item->id) selected @endif> {{$item->title}} </option>
                       @endforeach
                    </select>
                </div>
            </div>

             
       

          
            <div class="col-md-3">
                <div class="form-group text-left">
                     <label>Valid <b class="text-danger">From</b> Date</label>
                     <input type="date" name="from_date" value="{{$product->start_from->toDateString()}}" class="form-control" required>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group text-left">
                     <label>Valid <b class="text-danger">From</b> Time</label>
                     <input type="time" name="from_time" value="{{$product->start_from->toTimeString()}}" class="form-control" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group text-left">
                     <label>Valid <b class="text-danger">To</b> Date</label>
                     <input type="date" name="to_date" value="{{$product->start_to->toDateString()}}" class="form-control" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group text-left">
                     <label>Valid <b class="text-danger">To</b> Time</label>
                     <input type="time" name="to_time"  value="{{$product->start_to->toTimeString()}}" class="form-control" required>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Bid Image</label>
                    <input type="file" class="form-control" id="upload_img" name="temp_image" style="display:none" onchange="document.getElementById('productimg_file').innerHTML = this.files[0].name"  >
                    <p id="productimg_file"></p>
                    <button type="button" class="btn btn-primary btn-sm" onclick="document.getElementById('upload_img').click()">Upload Image <i class="fa fa-image"></i> </button>
                </div>
        </div>

            <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Bid Landing Image</label>
                        <input type="file" class="form-control" id="upload_img_landing" name="temp_image_landing" style="display:none" onchange="document.getElementById('productimg_landing_file').innerHTML = this.files[0].name"  >
                        <p id="productimg_landing_file"></p>
                        <button type="button" class="btn btn-primary btn-sm" onclick="document.getElementById('upload_img_landing').click()">Upload Image <i class="fa fa-image"></i> </button>
                    </div>
            </div>
            

               <input type="hidden" name="id" value="{{$product->id}}">

               <div class="col-md-12">
                    @if($product->image)
                       <img src="{{AWS_URL($product->image)}}" style="max-width:200px"  alt="">  
                    @endif
                    @if($product->image_landing)
                       <img src="{{AWS_URL($product->image_landing)}}" style="max-width:200px"  alt="">  
                    @endif
               </div>

                   
               <div class="col-md-12 text-left ">
                    <input class="form-check-input"  id="pooular" name="pooular"  type="checkbox" value="" checked="">
                    <label class="form-check-label" for="pooular"> Make It Popular  </label>
                </div>


               <div class="col-md-12 mt-2">
                   <div class="form-group text-left">
                       <button class="btn btn-primary">
                          Update
                       </button>
                   </div>
               </div>
            </div>
         </form>
    </div>
 </div>

@endsection


@section('scripts')
  @include($inc."nq_script")
@endsection