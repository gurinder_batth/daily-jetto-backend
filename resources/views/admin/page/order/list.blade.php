@extends($ADMIN_EXTEND)
@section('content')

<form action="" class="m-4" name="order_filter">
   <div class="container">
      <div class="row">
         <div class="col-md-9"></div>
          <div class="col-md-3">
              @php $status = [ 'pending' , 'paid' , 'accept' , 'delievered' , 'canceled'  ]  @endphp
              <label for="">Select Order Status</label>
               <select name="status" id="" class="form-control" onchange="document.order_filter.submit()" required>
                   <option value="all">All</option>
                    @foreach ($status as $item)
                        <option value="{{$item}}" @if($item == request()->get('status')) selected @endif> {{strtoupper($item)}}</option>
                    @endforeach
               </select>
         </div>
      </div>
      @if(count(request()->all()) > 0)
         <div class="row">
            <div class="col-md-12 text-right mt-2">
            <a href="{{a_route('order.list')}}" class="text-danger"><b> <i class="fa fa-filter"></i> Clear Filter</b></a>
            </div>
         </div>
      @endif
   </div>
</form>

 <div class="card">
      <div class="card-header card-header-success">
            <h4 class="card-title "> <i class="fa fa-shopping-cart"></i> &nbsp Order List</h4>
            <p class="card-category">Latest order list from Planet Vegan APP</p>
            <div class="text-right">
            </div>
      </div>

    <div class="card-body">
      

       {{$orders->links()}}

       <div class="table-responsive">
            <table class="table  text-center table-striped ">
                <tr class="bg-primary text-white">
                   <th>Order  <i class="fa fa-shopping-cart"></i> </th>
                   {{-- <th>Paid    <i class="fa fa-money"></i> </th> --}}
                   <th>Items    <i class="fa fa-list"></i> </th>
                   <th>Number    <i class="fa fa-list"></i> </th>
                   <th>Total <i class="fa fa-dollar"></i> </th>
                   <th>Order    <i class="fa fa-user"></i> </th>
                   {{-- <th>Status   <i class="fa fa-info"></i> </th> --}}
                   <th>More   <i class="fa fa-info"></i> </th>
                </tr>
                  @if($orders->total() > 0)
                     @foreach ($orders as $order)
                     <tr style="font-size:12px">
                        <td class="text-left"> 
                        <a @if($order->order_numeric_status > 1) href="{{a_route('order.inovice',['id' => $order->id])}}" target="_blank" @else href="#" @endif class="text-primary"> <strong>#{{$order->id}}</strong>
                           @if($order->order_numeric_status > 1)  <i class="fa fa-print"></i> @endif
                              {{$order->created_at->format("M-d-y")}}    {{$order->created_at->format("h:i:a")}}  </a> </td> 
                           {{-- <td    data-toggle="tooltip" data-placement="top" title="Payment Id : {{$order->payment_id}}">
                           @if($order->payment_recieved > 0)
                              <p> <b>${{$order->payment_recieved}}</b> </p>
                           @else
                           <span class="text-danger">Not yet</span>
                           @endif --}}
                        </td>
                        <td > 
                              {{$order->product->title ?? ""}}
                        </td>
                        <td > 
                              {{$order->number ?? ""}}
                        </td>
                        <td> {{$order->total_price}}   </td>
                        <td>{{$order->user->name ?? ""}} - {{$order->user->mobile ?? ""}}</td>
                        {{-- <td class="text-right">
                               @if($order->driver_id == null)
                                 <button 
                                    onclick='acceptOrder(@json($order))'
                                    data-toggle="modal" data-target="#accept_order"
                                    data-toggle="tooltip" data-placement="top" title="Press button to Accept the Order #{{$order->id}}"
                                    class="btn btn-success btn-sm" style="padding: 0.40625rem 0.25rem;">
                                       <i class="fa fa-check"></i>
                                 </button>
                              @else
                              <div id="order_accepteds">
                                    {{strtoupper($order->status)}} 
                                 <div style="margin-top: -8px;">({{$order->driver->name ?? ""}})</div>
                              </div>
                              @endif
                        </td> --}}


                        <td>
                           <a href="{{a_route('order.single',['id' => $order->id])}}"
                              data-toggle="tooltip" data-placement="top" title="Know the detailed info of #{{$order->id}}"
                              class="btn  btn-sm" style="padding: 0.40625rem 0.75rem;">
                              <i class="fa fa-info"></i>
                           </a>
                        </td>
                     </tr>
                     @endforeach
                  @else
                   <tr class="text-center text-uppercase text-weight-bold">
                      <td colspan="10">
                          <p>No Data Result Found :)</p>
                      </td>
                   </tr>
                  @endif
            </table>
       </div>

       {{$orders->links()}}
    </div>

 </div>

 @include($inc.'accept_order' , ['drivers' => $drivers])

@endsection

@section('scripts')
    <script>
        function acceptOrder(order){
            const _o = order
            const order_content = document.getElementById("order_content")
            const order_id = document.getElementById("order_id")
            const order_id_db = document.getElementById("order_id_db")
            order_id_db.value = _o.id
            const html = `
               <p><b>#Order ID</b>: ${_o.id}</p>
               <p><b>Shipping Addresss</b>: ${_o.shipping_address}</p>
               <p><b>Payment</b>: $${_o.payment_recieved}</p>
               <p><b>Customer</b>: ${_o.user.name} - ${_o.user.mobile}</p>
            `
            order_content.innerHTML = html
            order_id.innerHTML = "#"+_o.id
        }
    </script>
@endsection