<div class="sidebar" data-color="purple" 
 data-background-color="white" style="overflow:hidden;"
 data-image="{{URL('public/new-admin-master/assets/img/sidebar-1.jpg')}}">
  
  <style>
 .sidebar .nav li a, .sidebar .nav li .dropdown-menu a {
        color: white;
        font-weight: bold;
        letter-spacing: 1px;
        text-transform: uppercase;
    }
  
 .dropdown-item{
        color: black !important;
    }
  
  </style>

    <div class="logo" style="background: #77c323;">
        <a href="{{URL('/')}}"  class="simple-text  logo-normal">
            <img src="{{URL('public/logo.png')}}" alt="" style="width:100px">
        </a>
    </div>

    <div class="sidebar-wrapper" style="background: #070909;">
        <ul class="nav" style="margin-bottom: 4rem">

         
            <li class="nav-item  @yield('dashboard-link')">
                <a class="nav-link" href="{{a_route('dashboard')}}">
                    <i class="material-icons text-primary">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
         
            <li class="nav-item  @yield('dashboard-link')">
                <a class="nav-link" href="{{a_route('slider.index')}}">
                    <i class="material-icons text-primary">image</i>
                    <p>Slider</p>
                </a>
            </li>

            <li class="nav-item dropdown @yield('enquires')">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons text-success">dashboard</i>
                            <span class="f15"> BIDS </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{a_route("product.add")}}"> <i class="fa fa-plus text-primary"></i> ADD</a>
                        <a class="dropdown-item" href="{{a_route("product.list")}}"> <i class="fa fa-list text-primary"></i> List</a>
                    </div>
             </li>

             <li class="nav-item dropdown @yield('enquires')">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons text-success">dashboard</i>
                        <span class="f15"> Winner </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{a_route("close_bid.winner")}}"> <i class="fa fa-list text-primary"></i> List</a>
                </div>
         </li>


            <li class="nav-item dropdown @yield('enquires')">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons text-warning">dashboard</i>
                            <span class="f15"> Categories </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{a_route("menu.add")}}"> <i class="fa fa-plus text-primary"></i> ADD</a>
                        <a class="dropdown-item" href="{{a_route("menu.list")}}"> <i class="fa fa-list text-primary"></i> List</a>
                    </div>
             </li>
            <li class="nav-item dropdown @yield('enquires')">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons text-danger">person</i>
                            <span class="f15"> User </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{a_route("user.list")}}"> <i class="fa fa-list text-primary"></i> List</a>
                    </div>
             </li>

            <li class="nav-item dropdown @yield('enquires')">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons text-primary">dashboard</i>
                            <span class="f15"> Order </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{a_route("order.list",['status' => 'all'])}}"> <i class="fa fa-list text-primary"></i> List</a>
                    </div>
             </li>

            {{-- <li class="nav-item dropdown @yield('enquires')">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons text-warning">two_wheeler</i>
                            <span class="f15"> Driver </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{a_route("driver.list")}}"> <i class="fa fa-list text-primary"></i> List</a>
                        <a class="dropdown-item" href="{{a_route("driver.add")}}"> <i class="fa fa-plus text-primary"></i> Add</a>
                    </div>
             </li> --}}


        </ul>
    </div>
</div>