<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daily Jeeto</title>
    <link rel="stylesheet" href="{{URL('public')}}/front/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{URL('public')}}/front/css/style.css">
</head>

<body>
    <div class="bg-color">
        <div class="container">
            <a href="">
                <img src="{{URL('public')}}/front/images/logo.png" class="brand-logo" alt="">
            </a>
            <div class="row mt-50">
                <div class="col-md-6">
                    <img src="{{URL('public')}}/front/images/banner.png" class="img-fluid" alt="">
                </div>
                <div class="col-md-6 text-ce mt-50">
                    <h4 class="title">Download the official Daily Jeeto app</h4>
                    <div class="form-group">
                        <input type="text" placeholder="Enter Your Mobile">
                    </div>
                    <button type="submit" class="btn btn-success">Get App Link</button>

                    <div class="row mt-20">

                        <div class="col-8 col-md-10">
                            <img src="{{URL('public')}}/front/images/qrcode.png" class="pull-right" width="100" alt="">
                            <div class="pull-right">
                                Scan the QR code to download <br> the official Daily Jeeto App
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb30">
                    <h3>Follow these steps to install the Daily Jeeto Android App</h3>
                    <img src="{{URL('public')}}/front/images/icon.webp" style="margin-top: 10px;" width="200" alt="">
                </div>
                <div class="col-md-4 mg-20">
                    <img src="{{URL('public')}}/front/images/install01.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 mg-20">
                    <img src="{{URL('public')}}/front/images/install02.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4 mg-20">
                    <img src="{{URL('public')}}/front/images/install03.jpg" class="img-fluid" alt="">
                </div>
            </div>

            <div class="row" style="margin-top: 50px;">
                <div class="col-md-12 text-center">
                    <h3 style="margin-bottom: 20px;">App Store Rating</h3>
                    <img src="{{URL('public')}}/front/images/daily.png" width="447px" alt="">
                </div>
            </div>
        </div>


    </section>

    <section class="bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="{{URL('public')}}/front/images/04.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-3">
                    <img src="{{URL('public')}}/front/images/05.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-3">
                    <img src="{{URL('public')}}/front/images/06.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-3">
                    <img src="{{URL('public')}}/front/images/07.jpg" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4 style="margin-bottom: 20px">Download the Official App</h4>
                    <a href=""> <img src="{{URL('public')}}/front/images/down.webp" width="170px" alt=""></a>
                </div>
            </div>
        </div>
    </section>

    <div class="bg">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-12">

                    &copy; Privacy Policy Daily Jeeto

                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="{{URL('public')}}/front/js/bootstrap.min.js"></script>
</body>

</html>
