<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\MenuController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\DriverController;
use App\Http\Controllers\Api\WalletController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Api\RazroPayController;
use App\Http\Controllers\Admin\CloseBidController;
use App\Http\Controllers\Api\NotificationController;

Route::get("random-order-winner",[CloseBidController::class,'checkBid']);


Route::prefix("user")->group(function(){
    Route::name("user.")->group(function(){
        Route::post("signup",[ UserController::class , "signup" ])->name("signup");
        Route::post("login",[ UserController::class , "login" ])->name("login");
        Route::post("forget-password",[ UserController::class , "forgetPassword" ])->name("forget.password");
        Route::post("reset-password",[ UserController::class , "resetPassword" ])->name("reset.password");
    });
});

Route::prefix("auth")->group(function(){
        Route::post("current-user",[ UserController::class , "currentUser" ]);
});


Route::prefix("product")->group(function(){
    Route::name("product.")->group(function(){
        Route::post("single",[ ProductController::class , "single" ]);
        Route::post("search",[ ProductController::class , "search" ]);
        Route::post("popular",[ ProductController::class , "popular" ]);
        Route::post("list",[ ProductController::class , "list" ]);
        Route::post("list-by-menu",[ ProductController::class , "listByMenu" ]);
    });
});



Route::prefix("razro_pay")->group(function(){
    Route::name("razropay.")->group(function(){
        Route::post("create",[ RazroPayController::class , "createOrder" ]);
        Route::post("verify-payment",[ RazroPayController::class , "verifyPayment" ]);
    });
});

Route::prefix("driver")->group(function(){
    Route::name("driver.")->group(function(){
        Route::post("order-list",[ DriverController::class , "orderList" ]);
        Route::post("status-list",[ DriverController::class , "statusList" ]);
        Route::post("upda te-current-location",[ DriverController::class , "updateCurrentLocation" ]);
    });
});

Route::prefix("slider")->group(function(){
    Route::name("slider.")->group(function(){
        Route::get("list",[ SliderController::class , "list" ]);
    });
});

Route::prefix("home")->group(function(){
    Route::post("update-fcm",[ HomeController::class , "upateFcmToken" ]);
    Route::post("update-fcm-driver",[ HomeController::class , "upateDriverFcmToken" ]);
    Route::post("update-address",[ HomeController::class , "updateAddress" ]);
});

Route::prefix("order")->group(function(){
    Route::name("order.")->group(function(){
        Route::post("make",[ OrderController::class , "make" ]);
        Route::post("list",[ OrderController::class , "list" ]);
        Route::post("list-product",[ OrderController::class , "listProduct" ]);
        Route::post("on-the-way",[ OrderController::class , "onTheWay" ]);
        Route::post("delivered-order",[ OrderController::class , "deliveredOrder" ]);
        Route::post("cancel-order",[ OrderController::class , "canceledOrder" ]);
        Route::post("rating",[ OrderController::class , "rating" ]);
        Route::post("driver-location",[ OrderController::class , "driverLocation" ]);
    });
});

Route::prefix("payment")->group(function(){
    Route::name("payment.")->group(function(){
        Route::post("charge",[ PaymentController::class , "charge" ]);
    });
});

Route::prefix("notification")->group(function(){
    Route::name("notification.")->group(function(){
        Route::get("/",[ NotificationController::class , "index" ]);
    });
});

Route::prefix("wallet")->group(function(){
    Route::name("wallet.")->group(function(){
        Route::post("create",[ WalletController::class , "index" ]);
        Route::post("list",[ WalletController::class , "list" ]);
        Route::post("current",[ WalletController::class , "current" ]);
    });
});

Route::prefix("menu")->group(function(){
    Route::name("menu.")->group(function(){
        Route::post("list",[ MenuController::class , "list" ]);
    });
});