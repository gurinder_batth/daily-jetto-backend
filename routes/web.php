<?php

use Illuminate\Support\Facades\Route;
use App\PushNotification\PushNotification;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\FacebookLoginController;


Route::get("notification",function(){
     class Test {
           use PushNotification;
           public function __construct()
           {
               $data = [
                    "cOk9ar5FQxqUKPY61i0680:APA91bERyp6KmGypE9pGqZkjUChL8uC7wLx74HHglCQZqxPHJvZ3wn4ptHvJp6IZpxjQrD6N7goWs7rPk8fJNfSrMRbi0VEAi00Rpm7HlORvdnvQ90Nn7gNJLybxc2dJewMvuRce3NPH"
               ];
              dd( $this->sendNotificationTopic("Game","Check") );
           }
     }
     $test = new Test();
});

Route::get("/",[HomeController::class,'index']);

Route::prefix("admin")->group(function(){
    Route::name("admin.")->group(function(){
         Route::get("login",[UserController::class,"login"])->name("login");
         Route::post("login-check",[UserController::class,"loginCheck"])->name("login.check");
         Route::get("logout",[UserController::class,"logout"])->name("logout");
         Route::middleware("is_admin")->group(function(){
              require_once 'admin-routes.php';
         });
    });
});

Route::prefix("facebook")->group(function(){
   Route::get("login",[FacebookLoginController::class,"index"]);
   Route::get("setup",[FacebookLoginController::class,"setup"]);
   Route::get("callback",[FacebookLoginController::class,"callback"])->name("fb.callback");
   Route::get("delete",[FacebookLoginController::class,"delete"])->name("fb.delete");
});