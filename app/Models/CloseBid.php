<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Model\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CloseBid extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'product_id' ,
        'order_id'
    ];

    public function order()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
