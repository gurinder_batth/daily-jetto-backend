<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationUser extends Model
{
    use HasFactory;
    protected $appends = [ 'notification_at' , 'img_src'];
    public function getNotificationAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }
    public function getImgSrcAttribute()
    {
        return "https://i.pinimg.com/736x/2f/e1/f8/2fe1f88fd7c7e398dee3fa97ab115f30.jpg";
        if($this->image != null){
            return "https://i.pinimg.com/736x/2f/e1/f8/2fe1f88fd7c7e398dee3fa97ab115f30.jpg";
        }
        return null;
    }
}
