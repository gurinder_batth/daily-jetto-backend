<?php

namespace App\Models\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;
    protected $fillable = ['path'];
    protected $appends = ['img_path'];

    public function getImgPathAttribute($val)
    {
        return URL("storage/app/")."/".$this->path;
    }
}
