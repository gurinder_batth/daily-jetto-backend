<?php

namespace App\Models\Model;

use App\Models\User;
use App\Models\Driver;
use App\Models\Wallet;
use App\Models\Product;
use App\Models\Model\CartProcessing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
          "total_price" , "user_id" ,
          "payment_recieved" , "payment_id" ,
          "product_id" , "number" 
    ];

    protected $appends = [
        "bet_at"
    ];

    public function getBetAtAttribute()
    {
        if($this->created_at){
            return $this->created_at->format("h:i A d-M-Y");
        }
        return "--";
    }

    /**
     **$status  => pending , paid , accept , delievered , canceled 
     */
    public function setOrderPaid(){
        $this->status = "paid";
    }
    
    public function getOrderDateAttribute($val)
    {
        return $this->created_at->format("M d,Y");
    }
    
    public function getOrderDateFullAttribute($val)
    {
        return $this->created_at->format("M d,Y h:i a");
    }
    
    public function getTypeUserAttribute($val){
        return "driver";
    }
    public function getPickFromAttribute($val){
        return "Planet Vegan";
    }

    public function getOrderNumericStatusAttribute($val){
         $status = 1;
         switch($this->status){
            case "pending":
                 $status = 1;
            break;
            case "paid":
                 $status = 2;
            break;
            case "accept":
                 $status = 3;
            break;
            case "delievered":
                 $status = 4;
            break;
            case "canceled":
                 $status = 4;
            break;
        }


        return $status;

    }

    public function setOrderAccept(Driver $driver){
        $this->driver_id = $driver->id;
        $this->status = "accept";
    }

    public function setOrderAcceptNotification(){
        $message = "Your Order Number ".$this->id ." of $".$this->payment_recieved." successfully Accepted";
        $title = "Hi! ". $this->user->name." your order on the way";
        $this->user->sendNotification($title, $message);
    }
    public function orderDelieveredUserNotification(){
        $message = "Your Order Number ".$this->id ." of $".$this->payment_recieved." successfully Delievered";
        $title = "#".$this->id." Order Delievered Successfully";
        $this->user->sendNotification($title, $message);
    }
    public function orderCanceledUserNotification(){
        $message = "Your Order Number ".$this->id ." of $".$this->payment_recieved." Canceled";
        $title = "#".$this->id." Order Canceled";
        $this->user->sendNotification($title, $message);
    }

    public function setOrderAcceptNotificationDriver(){
        $message = "Address : " . $this->shipping_address;
        $title = "Hi! ". $this->driver->name." you have need to deliver new order.";
        $this->driver->sendNotification($title, $message);
    }

    public function orderDelieveredDriverNotification(){
        $message = "Thank You! #" .  $this->id . " Order has successfully delievered";
        $title = "Thank You! Order has delievered";
        $this->driver->sendNotification($title, $message);
    }
    public function orderCanceledDriverNotification(){
        $message = "Oops! #" .  $this->id . " Order has Canceled";
        $title = "Order has Canceled";
        $this->driver->sendNotification($title, $message);
    }

    public function setOrderDelievered(){
        $this->status = "delievered";
    }
    public function setOrderCanceled($reason){
        $this->status = "canceled";
        $this->cancel_reason = $reason;
    }

    public function cart(){
       return $this->hasMany(CartProcessing::class,"order_id","id");
    }
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
    public function driver(){
        return $this->hasOne(Driver::class,'id','driver_id');
    }
    
    public function hasActiveStatus($status){
       return $this->order_numeric_status >= $status ? "active" : "";
    }

    public function detectBidNumberAmount()
    {
        $user = $this->user;
        $user->wallet -= $this->number;
        $user->save();
        Wallet::create([
            "amount" => -1 * $this->number ,
            "user_id" => $user->id ,
            "reason" => $this->product->title . " for winning" ,
         ]);
    }

}
