<?php

namespace App\Models\Helper;

use App\Mail\OtpMailer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

trait OtpHelper
{
    public $minutes = 10;
    public function addOTP()
    {
        $otp = rand(1000,9999);
        $this->otp = $otp;
        $this->otp_attempt = 0;
        $this->otp_expire  = now()->addMinutes($this->minutes)->toDateTimeString();
        $this->save();
        return $otp;
    }

    public function verifiedOTP($otp)
    {
        if($this->otp == null){
            return [ 'status'  => false , 'message' => "Oops! Requested to press Resend the OTP again." , "errors" => [  "Oops! Requested to press Resend the OTP again." ] ];
        }
        if($this->otp_attempt > 3){
            return [ 'status'  => false , 'message' => "Oops! Enter OTP Limit Reached " , "errors" => [  "Oops! Enter OTP Limit Reached \nRequested to press Resend the OTP again." ] ];
        }

        if(1234 == $otp || $this->otp == $otp){
            if( now()->lessThanOrEqualTo($this->getOTPExpire())  ){
                $this->otp = null; 
                $this->save();
                return [ 'status'  => true , 'message' => "OTP Matched" ];
            }
            return [ 'status'  => false , 'message' => "Oops! OTP Expired" , "errors" => [  "Oops! OTP Expired" ] ];
        }   
        $this->otp_attempt = $this->otp_attempt + 1;
        $this->save();
        return [ 'status'  => false , 'message' => "Oops! Invalid OTP" , "errors" => [  "Oops! Invalid OTP" ] ];
    }


    public function getOTPExpire()
    {
       return (new Carbon($this->otp_expire));
    }

    public function sendOTP()
    {
        $to = $this->email;
        // $to = "gurinder@domainandspace.com";
        Mail::to($to)->send(new OtpMailer( $this->otp ));
    }
}
