<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WalletPayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'amount'   ,
        'payment_id'   ,
        'user_id'
    ];
}
