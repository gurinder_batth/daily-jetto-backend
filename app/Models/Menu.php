<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $fillable = [
        "title" , "description" , "image" ,"trash"
    ];
    protected $appends = [
        "img_path" 
    ];

    public function getImgPathAttribute(){
        return AWS_URL($this->image);
    }

}
