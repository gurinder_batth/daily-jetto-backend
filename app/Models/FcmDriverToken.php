<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FcmDriverToken extends Model
{
    use HasFactory;
    protected $fillable = ['driver_id','token'];
}
