<?php
namespace App\Http\Controllers\Admin;

use App\Models\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    public function add(Request $req) {
        return ADMIN_VIEW("driver.add");
    }

    public function create(Request $req) {
        $req->validate([
            'name' => "required|max:255",
            'email' => "required|max:255|unique:users|unique:drivers",
            'mobile' => "required|max:30|unique:users|unique:drivers",
            'vehicle_name' => "nullable|max:255",
            'vehicle_number' => "nullable|max:255",
            'password' => "required|min:4|max:16|confirmed",
        ]);
        $req->merge(['password' => bcrypt($req->password)]);
        $driver = Driver::create($req->all());
        session()->flash("success","Driver successfully created!");
        return redirect()->to(a_route('driver.list'));
    }

    public function list(Request $req){
        $drivers = Driver::latest()->paginate(10);
        return ADMIN_VIEW("driver.list")->with(['drivers' => $drivers]);
    }
}
