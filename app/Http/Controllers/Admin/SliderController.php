<?php

namespace App\Http\Controllers\Admin;

use App\Models\Model\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{

    
    public function index(Request $req)
    {
        $list = Slider::latest()->paginate(10);
        return ADMIN_VIEW("slider.index")->with(['list' => $list]);
    }

    public function list(Request $req)
    {
        $list = Slider::whereTrash(0)->latest()->take(10)->get();
        return response()->json(['list' => $list]);
    }


    public function create(Request $req)
    {
        $req->validate([
            "temp_image" => "required|file",
         ]);
        $slider = Slider::create([
            "path" => $req->temp_image->store("sliders/".time())
        ]);
        session()->flash("success","Thank You!");
        return redirect()->back();
    }

    public function trash(Request $req)
    {
        $slider = Slider::find($req->id);
        $slider->trash = $req->flag;
        $slider->save();
        session()->flash("success","Thank You!");
        return redirect()->back();
    }
}
