<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function add(Request $req) {
        $menu = Menu::whereTrash(0)->get();
        return ADMIN_VIEW("product.add", ['menu' => $menu]);
    }
    public function create(Request $req) {

            $req->validate([
               "title" => "required|max:255",
               "description" => "required|max:65555",
               "price" => "required|numeric",
               "temp_image" => "required|file",
            ]);

            $from = new Carbon($req->from_date." ".$req->from_time);
            $to = new Carbon($req->to_date." ".$req->to_time);

            $req->merge([
                "start_from" => $from->toDateTimeString() ,
                "start_to" => $to->toDateTimeString() ,
            ]);


            $product = Product::create($req->all());
            $name  = $req->temp_image->store("menu_images/".time());
            $product->image_landing = $req->temp_image->store("menu_images_landing/".time());
            $product->image =  $name;
            $product->save();
            return redirect()->route("admin.product.list");
    }

    public function list(Request $req)
    {
        $list = Product::whereTrash(0)->latest()->paginate(20);
        return ADMIN_VIEW("product.list", ['list' => $list ]);
    }

    public function delete(Request $req){
        $menu = Product::find($req->id);
        $menu->trash = $req->flag;
        $menu->save();
        return redirect()->back();
    }
    public function edit(Request $req){
        $product = Product::find($req->id);
        $menu = Menu::whereTrash(0)->get();
        return ADMIN_VIEW("product.edit" ,["product" => $product , 'menu' => $menu]);
    }

    public function update(Request $req){
        $req->validate([
              "title" => "required|max:255",
              "description" => "required|max:65555",
              "price" => "required|numeric"
         ]);
         $from = new Carbon($req->from_date." ".$req->from_time);
         $to = new Carbon($req->to_date." ".$req->to_time);
         $req->merge([
             "start_from" => $from->toDateTimeString() ,
             "start_to" => $to->toDateTimeString() ,
         ]);
        $product = Product::find($req->id);
        $product->update($req->all());
        if($req->temp_image){
            $name  = $req->temp_image->store("menu_images/".time());
            $product->image = $name;
            $product->save();
        }
        if($req->temp_image_landing){
            $name  = $req->temp_image_landing->store("menu_images_landing/".time());
            $product->image_landing = $name;
            $product->save();
        }
        $product->save();
        return redirect()->route("admin.product.list");
    }
}
