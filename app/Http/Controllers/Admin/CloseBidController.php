<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\CloseBid;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CloseBidController extends Controller
{
      public function close(Request $req)
      {
            $req->validate([
                'id' => 'required|numeric|exists:products,id' 
            ]);
            
            $close = CloseBid::whereProductId($req->id)->first();

            if($close != null){
                session()->flash("unsuccess","Oops! Bid is already closed");
                return redirect()->back();
            }
            $order = Order::orderBy('number','ASC')->whereProductId($req->id)->first();

            if($order != null){
               $order->detectBidNumberAmount();
                CloseBid::create([
                    'user_id' => $order->user_id,
                    'product_id' =>  $order->product_id ,
                    'order_id' => $order->id
                ]);
                session()->flash("success","Bid closed successfully");
                return redirect()->back();  
            }
            
            Session()->flash("unsuccess","Oops! Zero bid found on this product");
            return redirect()->back();
      }

      public function winner(Request $req)
      {
           $list = CloseBid::latest()->paginate(10);
           return ADMIN_VIEW("closebid.winner")->withList($list);
      }

      public function checkBid()
      {
           $product = $this->getSingleProductWinner();
           if($product != null){
               $bidprice = $product->price;
               $earn = $product->orders->sum('total_price');
               if($earn < $bidprice){
                //    $product->start_to->day = now()->day;
                   $product->updateStartToDate();
                //    $product->start_to = $date;
                //    $product->start_to->setDay(1);
                   $product->save();

                   dd($product);
                   return response()->json(['message' => 'bid time increase']);
               }
               else
               {
                 $order = Order::orderBy('number','ASC')->whereProductId($product->id)->first();
                 if($order != null){
                     $order->detectBidNumberAmount();
                     $closebid = CloseBid::create([
                         'user_id' => $order->user_id,
                         'product_id' =>  $order->product_id ,
                         'order_id' => $order->id
                     ]);
                     return response()->json(['order' => $order , 'bid' => $closebid ]);
                 }
               }
           }
           return response()->json(['message' => 'no bid found']);
      }

      public function getSingleProductWinner()
      {
             $date = now();
             return Product::whereDate("start_to","<=",$date->toDateTimeString())
                        ->inRandomOrder()
                        ->whereDoesntHave("closed")
                        ->first();
      }
}

