<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function login(Request $req)
    {
       return ADMIN_VIEW("user.login");
    }
    
    public function logout(Request $req)
    {
         Auth::logout();
         return redirect()->back();
    }

    public function loginCheck(Request $req)
    {   
             $req->validate([
                 'email' => 'required|email', // make sure the email is an actual email
                 'password' => 'required'
             ]);
             
             $userdata = array(
                'email' => $req->email  ,
                'password' => $req->password ,
                'admin' => 1
              );

              // attempt to do the login
               if (Auth::attempt($userdata))
                {
                    return redirect()->to(a_route("dashboard"));
                }
                // validation not successful, send back to form
                session()->flash("unsuccess","Invalid username and password");
                return redirect()->back();
    }

    public function list(Request $req)
    {
        $list = User::latest()->paginate(10);
        return ADMIN_VIEW("user.list", ['list' => $list ]);
    }

    public function wallet(Request $req)
    {
        $wallet = Wallet::latest()->paginate(10);
        $user = User::find($req->id);
        return ADMIN_VIEW("user.wallet", ['user' => $user  , 'wallet' => $wallet ]);
    }

    public function walletCreate(Request $req)
    {
          $req->validate([
               'user_id' => 'required',
               'amount' => 'required|numeric|min:1' ,
               'reason' => 'required|min:1|max:255' ,
          ]);

          if($req->submit == "subtract"){
                $req->merge([ 'amount' =>  -1 * abs($req->amount) ]);
          }
          $wallet =  Wallet::create($req->all());
          $user = User::find($req->user_id);
          $user->wallet = $user->wallet + $wallet->amount;
          $user->save();
          return redirect()->back();
    }

}
