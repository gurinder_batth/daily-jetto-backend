<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Api\UserValidation;
use App\Mail\OtpMailer;

class UserController extends Controller
{
    use UserValidation;
    
     public function login(Request $req) {
          $err = $this->loginValidation($req);
          if($err){ return $err; }
          $user = Auth::attempt(['email' => $req->email , 'password' => $req->password]);
         
          if($user){
                $user = User::whereEmail($req->email)->first();
                return response()->json([
                    "user" => $user ,
                    "status" => true ,
                    "message" => "Login Successfully" ,
                ]);
          }

         
        return response()->json([
            "status" => false ,
            "errors" => [ "Invalid Username and Password" ] ,
            "message" => "Invalid Username and Password" ,
        ]);

          
     }    


     public function signup(Request $req)  {
          $err = $this->signUpValidation($req);
          if($err){ return $err; }
          
          $req->merge(['password' => bcrypt($req->password) ]);
          $user = User::create($req->all());
          $user->token = Str::random(60);
          $user->save();
          
          return response()->json([
               "status" => true ,
               "user" => $user  ,
               "message" => "Register Suceesfully" ,
          ]);
     }    

 

     public function currentUser(Request $req){
          $err = $this->currentUserValidation($req);
          if($err){ return $err; }
          $user = User::with("adr")->find($req->id);
          return response()->json([
               "status" => true ,
               "user" => $user  ,
               "message" => "Current User" ,
          ]);
     }

     public function forgetPassword(Request $req){
          $err = $this->userValidation($req);
          if($err){ return $err; }
          $user = User::whereEmail($req->email)->first();
          $user->addOTP();
          $user->sendOTP();
          return response()->json([
               "status" => true ,
               "user_id" => $user->id ,
               "message" => "Please check your email inbox for OTP" ,
          ]);
     }
     public function resetPassword(Request $req){
          $err = $this->resetPasswordValidation($req);
          if($err){ return $err; }
          $user = User::find($req->user_id);

          /**
           * TODO: verifiedOTP return true if OTP Matched Otherwise Stop go for Further Execution
           */
          $res = $user->verifiedOTP($req->otp);
          if($res['status'] == false){ return $res; }

          $user->password = bcrypt($req->password);
          $user->save();

          return response()->json([
               "status" => true ,
               "user" => $user  ,
               "address" => $user->adr->first() ,
               "message" => "Please check your email inbox for OTP" ,
          ]);
     }

}
