<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\NotificationUser;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function index()
    {
        $notification = NotificationUser::latest()->get();
        return response()->json(['notification' => $notification]);
    }
}
