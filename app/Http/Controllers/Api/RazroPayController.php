<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\User;
use Razorpay\Api\Api;
use App\Models\Wallet;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\WalletPayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Razorpay\Api\Errors\SignatureVerificationError;

class RazroPayController extends Controller
{
     /**
      * TODO: Create order and return order id to user side
      *
      * @param Request $req
      * @return void
      */
     public function createOrder(Request $req,Api $api)
     {
           $err = $this->isValidEntry($req);
           if($err){ return $err; }
           $response  = $api->order->create([
                'receipt' => "Add money request" ,
               'amount'  => $req->amount * 10 ,
               'currency' => 'INR'
           ]);

           $wallet = WalletPayment::create([
               'amount'  => $response['amount_due'] / 10  ,
               'payment_id'  => $response['id'] ,
               'user_id' => $req->user_id ,
           ]);  

           $id = $response->id;
           return response()->json(['wallet' => $wallet , 'status' => true ]);
     }

     public function isValidEntry(Request $req)
     {
           $validator = Validator::make($req->all(),[
                "amount" => "required|numeric|min:10" ,
                "user_id" => "required|exists:users,id" ,
            ]);

            if( $validator->fails() ){
                return response()->json([
                    "status" => false  ,
                    "errors" => $validator->errors()->all() ,
                    "message" => "Oops! invalid data"
                ]);
            }

            return false;
     }

     public function isValidVerification(Request $req)
     {

          

           $validator = Validator::make($req->all(),[
                "order_id" => "required|max:255|exists:wallet_payments,payment_id" ,
                "payment_id" => "required|max:255" ,
            ]);
            

            if( $validator->fails() ){
                return response()->json([
                    "status" => false  ,
                    "errors" => $validator->errors()->all() ,
                    "message" => "Oops! invalid data"
                ]);
            }

            return false;
     }
    
     public function verifyPayment(Request $req,Api $api)
     {

        $err = $this->isValidVerification($req);
        if($err){ return $err; }

        $order_id =  $req->order_id;
        $payment_id = $req->payment_id;
        $generated_signature = hash_hmac('sha256', $order_id . "|" . $payment_id, env("RAZOR_API_SECRET"));
        $success = false;
        try
        {
            $attributes = array(
                'razorpay_order_id' => $order_id ,
                'razorpay_payment_id' => $payment_id ,
                'razorpay_signature' => $generated_signature
            );
    
            $api->utility->verifyPaymentSignature($attributes);
            $success = true;

            $walletpayment = WalletPayment::wherePaymentId($req->order_id)->wherePaid(0)->first();
            if($walletpayment == null){
                throw new SignatureVerificationError("Oops! Payment not valid");
            }
            $user = User::find($walletpayment->user_id);
            $wallet = Wallet::create([
                "amount" => $walletpayment->amount ,
                "user_id" => $user->id ,
                "reason" => "Added by payment gateway" ,
             ]);
             $user->wallet += (double) $wallet->amount;
             $user->save();
             $walletpayment->paid = 1;
             $walletpayment->save();

        }
        catch(SignatureVerificationError $e)
        {
            $success = false;
            $error = 'Razorpay Error : ' . $e->getMessage();
        }
       return response()->json(['success' => $success , 'user' => $user ?? '' ]);
     }
}
