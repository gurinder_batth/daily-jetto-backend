<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Driver;
use App\Models\Product;
use App\Models\Model\Order;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Model\CartProcessing;
use App\Http\Controllers\Api\UserValidation;

class OrderController extends Controller
{
    use UserValidation;
    public $order_total_price = 0;
    
    public function make(Request $req){
        $err = $this->orderValidation($req);
        if($err){ return $err; }
        $err = $this->isBidExist($req);
        if($err){ return $err; }
        $err = $this->isBidOpen($req);
        if($err){ return $err; }
        $err = $this->isSufficentBalance($req);
        if($err){ return $err; }
        $order = $this->processOrder($req);
        if($order) :
            $user = User::find($req->user_id);
            $user->wallet = $user->wallet - $order->total_price;
            $user->save();
            return response()->json(['order' => $order  , 'status' => true]);
        else :
            return response()->json([ 'status' => false ]);
        endif ;
    }
    
    public function isBidOpen(Request $req)
    {
         $product = Product::find($req->product_id);
         if($product->closed != null){
            return response()->json(['status' => false , 'errors' => [ 'Oops! Bid is closed' ] ]); 
         }
         return false;
    }

    public function isBidExist(Request $req)
    {
        $order_id = Order::where([ 
                "user_id" => $req->user_id ,
                'product_id' => $req->product_id ,
                'number' => $req->number , 
        ])->first();
        if($order_id != null ){
            return response()->json(['status' => false , 'errors' => [ 'Bid Already placed at this number' ] ]); 
        }
        return false;
    }


    public function isSufficentBalance(Request $req)
    {
         $user = User::find($req->user_id);
         $product = Product::find($req->product_id);
         if($user->wallet > $product->bidprice){
            return false;
         }
         return response()->json(['status' => false , 'errors' => [ 'Insufficent Balance' ] ]); 
    }

    public function processOrder(Request $req) {
        return Order::create([
                    "total_price"  => Product::find($req->product_id)->bidprice ,
                    "user_id" => $req->user_id ,
                    'product_id' => $req->product_id ,
                    'number' => $req->number ,
                    "payment_id" =>  "nwiji786ryh8467974w9",
                ]);
    }

    public function proccessCart(Array $products,Request $req,Order $order){
        $orders = 0;
        foreach($products as $product){
                $cp =  CartProcessing::create([
                        "order_id" => $order->id ,
                        "total_price" => $product['total_price'] ,
                        "single_price" => $product['single_price'] ,
                        "qun" => $product['qun'] ,
                        "product_id" => $product['product_id'] ,
                        "product_title" => $product['product_title'] ,
                        "user_id" => $req->user_id 
                ]);
                $orders++;
        }
        if( $orders > 0 ){
            return true;
        }
        return false;
    }


    public function list(Request $req){
        $err = $this->orderListValidation($req);
        if($err){ return $err; }
        $user = User::find($req->user_id);
        $items = Order::select('product_id')
                 ->with(['product','product.speficUserOrder','product.closed'])
                 ->distinct(['product_id'])
                 ->whereUserId($user->id)
                 ->orderBy("product_id","DESC")
                 ->paginate(20);
        return response()->json(['items' => $items , "status" => true]);
    }

    public function listProduct(Request $req){
        $err = $this->orderListValidation($req);
        if($err){ return $err; }
        $user = User::find($req->user_id);
        $items = Order::select('product_id')
                 ->with(['product','product.speficUserOrder','product.closed'])
                 ->distinct(['product_id'])
                 ->whereUserId($user->id)
                 ->orderBy("product_id","DESC")
                 ->paginate(20); 
        return response()->json(['items' => $items , "status" => true]);
    }

    public function deliveredOrder(Request $req){

        $err = $this->deliverOrderValidation($req);
        if($err){ return $err; }
        $driver = Driver::find($req->driver_id);
        $order = Order::find($req->order_id);
        /**
         * TODO: That's means driver not authorized to update this order Status
         * TODO: Because this order not belongs to this driver
         */
        if($order->driver_id != $driver->id){ return $this->unauthErr(); }
        $order->setOrderDelievered();
        $order->save();
        $order->orderDelieveredUserNotification();
        $order->orderDelieveredDriverNotification();
        return response()->json([
            "status" =>  true ,
            "message" => "Order has successfully delievered" ,
            'order' =>$order
        ]);
    }

    public function unauthErr(){
        return response()->json([
            "status" =>  false ,
            "errors" => [ "UnAuthrization" ],
            "message" => "UnAuthrization"
        ]);
    }

    public function canceledOrder(Request $req){
        $err = $this->deliverOrderValidation($req,["reason" => "required"]);
        if($err){ return $err; }
        $driver = Driver::find($req->driver_id);
        $order = Order::find($req->order_id);
        /**
         * TODO: That's means driver not authorized to update this order Status
         * TODO: Because this order not belongs to this driver
         */
        if($order->driver_id != $driver->id){ return $this->unauthErr(); }
        $order->setOrderCanceled($req->reason);
        $order->save();
        $order->orderCanceledUserNotification();
        $order->orderCanceledDriverNotification();
        return response()->json([
            "status" =>  true ,
            "message" => "Order has successfully canceled" ,
            'order' => $order
        ]);
    }

    public function onTheWay(Request $req){
        $err = $this->currentUserValidation($req);
        if($err){ return $err; }
        $list = Order::with(['driver'])->whereIn("status",["accept"])->whereUserId($req->id)->latest()->take(1)->get();
        return response()->json([
            "status" =>  true ,
            "message" => "On the way order list" ,
            'list' => $list
        ]);
    }

    public function rating(Request $req){
        $err = $this->ratingValidation($req);
        if($err){ return $err; }
        $order = CartProcessing::find($req->cart_id);
        $order->rating = $req->rating;
        $order->review = $req->review;
        $order->save();
        return response()->json([
            "status" =>  true ,
            "message" => "Thank You!" ,
            'order' => $order
        ]);
    }

    public function driverLocation(Request $req){
        $err = $this->driverLocationValidation($req);
        if($err){ return $err; }
        $driver = Driver::find($req->driver_id);
        $order = Order::find($req->order_id);
        $user = Order::find($req->user_id);
        if($order->driver_id != $driver->id){
            return response()->json([
                "status" => false ,
                "errors" => [ "UnAuth" ]
            ]);
        }
        return response()->json([
            "status" => true ,
            "data" => [
                "latitude" => $driver->latitude ,
                "longitude" => $driver->longitude ,
            ]
        ]);
    }
}
