<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function list(Request $req) {
        $list = Product::whereTrash(0)->whereDoesntHave("closed")->latest()->paginate(20);
        return response()->json(['list' => $list ,'status' => true ]);
    }
    
    public function search(Request $req) {
        $list = Product::whereTrash(0)->where("title","LIKE","%".$req->str."%")->take(10)->get();
        return response()->json(['list' => $list ,'status' => true ]);
    }
    public function popular(Request $req) {
        $list = Product::inRandomOrder()->whereTrash(0)->take(5)->get();
        return response()->json(['list' => $list ,'status' => true ]);
    }

    public function listByMenu(Request $req) {
        $list = Product::whereMenuId($req->menu_id)->whereTrash(0)->latest()->paginate(20);
        return response()->json(['list' => $list ,'status' => true ]);
    }

    public function single(Request $req) {
        $product = Product::find($req->id);
        return response()->json(['product' => $product ,'status' => true ]);
    }

}
