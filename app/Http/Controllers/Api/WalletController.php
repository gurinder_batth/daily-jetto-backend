<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    public function index(Request $req)
    {
        $req->validate([
            "amount" => "required|numeric",
            "user_id" => "required|numeric|exists:users,id"
        ]);

        $user = User::find($req->user_id);

        $wallet = Wallet::create([
            "amount" => $req->amount ,
            "user_id" => $req->user_id ,
            "reason" => "Added by payment gateway" ,
         ]);
         
         $user->wallet += (double) $wallet->amount;
         $user->save();

         return response()->json(['wallet' => $wallet ,'user' => $user ]);
    }

    public function list(Request $req)
    {
        $req->validate([
            "user_id" => "required|numeric|exists:users,id"
        ]);
        $wallet = Wallet::whereUserId($req->user_id)->latest()->paginate(20);
        return response()->json(['wallet' => $wallet ]);
    }
    public function current(Request $req)
    {
        $req->validate([
            "user_id" => "required|numeric|exists:users,id"
        ]);
        $user = User::find($req->user_id);
        return response()->json([ 'user' => $user ]);
    }
}
