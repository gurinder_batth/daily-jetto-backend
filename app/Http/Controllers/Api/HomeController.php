<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Driver;
use App\Models\UserAddress;
use App\Models\FcmUserToken;
use Illuminate\Http\Request;
use App\Models\FcmDriverToken;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    use UserValidation;

    public function upateFcmToken(Request $req) {
        $err = $this->tokenValidation($req,"users");
        if($err) { return $err; }
        $user = User::find($req->user_id);
        $fcm = FcmUserToken::updateOrCreate(
               ['user_id' => $user->id] ,
               ['token' => $req->token] ,
        );
        return response()->json(['status' => true , 'fcm' => $fcm ]);
    }

    public function upateDriverFcmToken(Request $req) {
        $err = $this->tokenValidation($req,"drivers");
        if($err) { return $err; }
        $user = Driver::find($req->user_id);
        $fcm = FcmDriverToken::updateOrCreate(
               ['driver_id' => $user->id] ,
               ['token' => $req->token] ,
        );
        return response()->json(['status' => true , 'fcm' => $fcm ]);
    }

    public function tokenValidation(Request $req,$table){
        $validator = Validator::make($req->all(),[
                   "user_id" => "required|exists:".$table.",id" ,
                    "token" => "required" ,
                ]);

        if( $validator->fails() ){
            return response()->json([
                "status" => false  ,
                "errors" => $validator->errors()->all() ,
                "message" => "Oops! invalid data"
            ]);
        }
        return false;
    }

    public function updateAddress(Request $req)
    {
        $err = $this->upateAddressValidation($req);
        if($err){ return $err; }
         /***TODO: After Validation */   
        $user = User::find($req->user_id);
        if($req->pick_current == false){
             $req->merge(['latitude' => null]);
             $req->merge(['longitude'  => null]);
             $req->merge(['street_address'  => $req->street_address ." ".$req->city ." " . $req->zip_code ]);
        }

        $address = UserAddress::updateOrCreate(
            ['user_id' => $user->id] ,
            $req->all()
        );

        return response()->json([
            "status" => true ,
            "address" => $address ,
            "message" => "Address Update Suceesfully" ,
       ]);
       
    }
    
}
