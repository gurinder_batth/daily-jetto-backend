<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait UserValidation
{

    public function loginValidation(Request $req)
    {
          $validator = Validator::make($req->all(),[
                    "email" => "required|email|min:3|max:255" ,
                    "password" => "required|min:3|max:255" ,
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }
    
    public function signUpValidation(Request $req)
    {

          $validator = Validator::make($req->all(),[
                    "name" => "required|min:3|max:60" ,
                    "email" => "required|email|min:3|max:255|unique:users|unique:drivers" ,
                    "mobile" => "required|min:8|max:60|unique:users|unique:drivers" ,
                    // "address" => "required|min:3|max:255" ,
                    "password" => "required|min:3|max:255|confirmed" ,
                    // "street_address" => "required|min:3|max:255",
                    // "city" => "required|min:3|max:255",
                    // "zip_code" => "required|min:3|max:255",
                    // "type" => "required|min:3|max:255",
                    // "pick_current" => "required",
                    // "latitude" => "required_if:pick_current,true",
                    // "longitude" => "required_if:pick_current,true",
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }
    public function userValidation(Request $req)
    {
          $validator = Validator::make($req->all(),[
                    "email" => "required|email|min:3|max:255|exists:users" ,
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }
    public function currentUserValidation(Request $req)
    {
          $validator = Validator::make($req->all(),[
                    "id" => "required|exists:users" ,
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }


    public function orderValidation(Request $req){
            $validator = Validator::make($req->all(),[
                "product_id" => "required|exists:products,id" ,
                "user_id" => "required|exists:users,id",
                "number" => "required|numeric",
            ]); 

            if( $validator->fails() ){
                return response()->json([
                    "status" => false  ,
                    "errors" => $validator->errors()->all() ,
                    "message" => "Oops! invalid data"
                ]);
            }

            return false; 
    }
    public function orderListValidation(Request $req){
            $validator = Validator::make($req->all(),[
                        "user_id" => "required|exists:users,id",
            ]); 

            if( $validator->fails() ){
                return response()->json([
                    "status" => false  ,
                    "errors" => $validator->errors()->all() ,
                    "message" => "Oops! invalid data"
                ]);
            }

            return false; 
    }

    public function deliverOrderValidation(Request $req,$custom_array = []) {
        $error_data = [
            "driver_id" => "required|exists:drivers,id",
            "order_id" => "required|exists:orders,id",
        ];
        if(count($custom_array) > 0){
          foreach($custom_array as $key => $value){
             $error_data[$key] = $value;
          }
        }
        $validator = Validator::make($req->all(),$error_data); 
        if( $validator->fails() ){
            return response()->json([
                "status" => false  ,
                "errors" => $validator->errors()->all() ,
                "message" => "Oops! invalid data"
            ]);
        }

        return false; 
    }

    public function ratingValidation(Request $req,$custom_array = []) {
        $error_data = [
            "cart_id" => "required|exists:cart_processings,id",
            "rating" => "required|numeric|min:1|max:5",
            "review" => "nullable|min:3|max:255",
            "user_id" => "required|exists:users,id",
        ];
        $validator = Validator::make($req->all(),$error_data); 
        if( $validator->fails() ){
            return response()->json([
                "status" => false  ,
                "errors" => $validator->errors()->all() ,
                "message" => "Oops! invalid data"
            ]);
        }
        return false; 
    }

    public function upateAddressValidation(Request $req)
    {

          $validator = Validator::make($req->all(),[
                    "user_id" => "required|exists:users,id",
                    // "street_address" => "required|min:3|max:255",
                    // "city" => "required|min:3|max:255",
                    // "zip_code" => "required|min:3|max:255",
                    // "type" => "required|min:3|max:255",
                    // "pick_current" => "required",
                    // "latitude" => "required_if:pick_current,true",
                    // "longitude" => "required_if:pick_current,true",
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }
    public function resetPasswordValidation(Request $req)
    {

          $validator = Validator::make($req->all(),[
                    "user_id" => "required|exists:users,id",
                    "otp" => "required|numeric",
                    "password" => "required|min:3|max:255|confirmed" ,
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }

          return false;
    }

    public function driverLocationValidation(Request $req,$custom_array = []) {
        $error_data = [
            "driver_id" => "required|exists:drivers,id",
            "order_id" => "required|exists:orders,id",
            "user_id" => "required|exists:users,id",
        ];
        $validator = Validator::make($req->all(),$error_data); 
        if( $validator->fails() ){
            return response()->json([
                "status" => false  ,
                "errors" => $validator->errors()->all() ,
                "message" => "Oops! invalid data"
            ]);
        }
        return false; 
    }

}
