<?php

namespace App\Http\Controllers\Api;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function list(Request $req) {
        $list = Menu::whereTrash(0)->latest()->paginate(20);
        return response()->json([ 'list' => $list ]);
    }
}
