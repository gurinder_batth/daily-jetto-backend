<?php

namespace App\Http\Controllers\Api;

use App\Models\Driver;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DriverController extends Controller
{
    public function orderList(Request $req){
        $err = $this->orderListValidation($req);
        if($err){  return $err; }
        $driver = Driver::find($req->driver_id);
        $orders = Order::with(['user'])->whereDriverId($driver->id)->latest()
                        ->whereStatus("accept")
                        ->whereDate("created_at",">=",now()->subDays(5)->toDateTimeString())
                        ->get();
        return response()->json(['orders' => $orders , 'status' => true ]);
    }

    public function statusList(Request $req){
        $err = $this->orderListValidation($req);
        if($err){  return $err; }
        $driver = Driver::find($req->driver_id);
        $orders = Order::with(['user'])->whereDriverId($driver->id)
                        ->whereIn("status",["canceled","delievered"])
                        ->latest()->paginate(20);
        return response()->json(['orders' => $orders , 'status' => true ]);
    }

    public function orderListValidation(Request $req){
        $validator = Validator::make($req->all(),[
                    "driver_id" => "required|exists:drivers,id",
        ]); 
        if( $validator->fails() ){
            return response()->json([
                "status" => false  ,
                "errors" => $validator->errors()->all() ,
                "message" => "Oops! invalid data"
            ]);
        }
        return false; 
    }

    public function updateCurrentLocation(Request $req){
        $err = $this->currentLocationValidation($req);
        if($err){  return $err; }
        $driver = Driver::find($req->driver_id);
        if( $driver->latitude == $req->latitude && $driver->longitude == $req->longitude){
            return response()->json(['status' => true  ,'messages' => "already updated"]); 
        }
        $driver->latitude = $req->latitude;
        $driver->longitude = $req->longitude;
        $driver->save();
        return response()->json(['status' => true  ,'messages' => "updated"]);
    }

    public function currentLocationValidation(Request $req){
          $validator = Validator::make($req->all(),[
                      "driver_id" => "required|exists:drivers,id",
                      "latitude" => "required",
                      "longitude" => "required",
            ]); 
            if( $validator->fails() ){
                return response()->json([
                    "status" => false  ,
                    "errors" => $validator->errors()->all() ,
                    "message" => "Oops! invalid data"
                ]);
            }
            return false; 
    }

}
