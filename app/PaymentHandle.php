<?php

namespace App;

class PaymentHandle
{
     private $total = 0;
     private $key;

     public function __construct(string $key)
     {
         $this->key = $key;
     }

     public function setTotal($total){
         $this->total += $total;
     }
     public function getTotal(){
         return $total;
     }
}
